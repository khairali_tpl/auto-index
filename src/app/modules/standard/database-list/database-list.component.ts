import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { MatSelectionList, MatListOption } from '@angular/material';
import { anim, shakeAnim, shakeMe } from '../../../animation/animation';
import { DbService } from '../../../services/db/db.service';
import { UtilService } from '../../../services/util/util.service';
import { IndexService } from 'src/app/services/index/index.service';
import { NotificationsService } from 'angular2-notifications';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-database-list',
  templateUrl: './database-list.component.html',
  styleUrls: ['./database-list.component.scss'],
  animations: [anim, shakeAnim]
})
export class DatabaseListComponent implements OnInit {
  @Input('database')
  public databaseList: string[];

  public visible = false;

  public state = 'state1';

  public shakeState = 'inactive';

  public selectedOption: MatListOption;

  @ViewChild(MatSelectionList)
  tableRef: MatSelectionList;

  constructor(
    public dbService: DbService,
    public notifyService: NotificationsService,
    public indexService: IndexService
  ) {}

  public ngOnInit(): void {
    this.animateMe();
  }

  public next(): void {
    if (this.selectedOption) {
      this.dbService.currentDB.dbName = this.getSelectedValue();
      this.indexService.currentIndex.databaseMeta.dbName = this.getSelectedValue();
      this.visible = !this.visible;
    } else {
      this.shakeState = shakeMe(this.shakeState);
      this.notifyService.error('Oops !', 'Choose database.');
    }
  }

  public changeSelect($evt): void {
    this.tableRef.deselectAll();
    this.tableRef.selectedOptions.toggle($evt.option);
    this.selectedOption = $evt.option;
  }

  public animateMe(): void {
    this.state = this.state === 'state1' ? 'state2' : 'state1';
  }

  private getSelectedValue(): string {
    return this.selectedOption.getLabel().trim();
  }
}
