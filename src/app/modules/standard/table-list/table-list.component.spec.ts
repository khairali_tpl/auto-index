import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbService } from '../../../services/db/db.service';
import { UtilService } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DatabaseListComponent } from '../database-list/database-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SchemaListComponent } from '../schema-list/schema-list.component';
import { TableListComponent } from '../table-list/table-list.component';
import { ColumnListComponent } from '../column-list/column-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DbInfo } from 'src/app/models/db-info.model';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationsService } from 'angular2-notifications';

describe('TableListComponent', () => {
  let component: TableListComponent;
  let fixture: ComponentFixture<TableListComponent>;
  let dbService: DbService;
  let mockDbInfo: DbInfo;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDividerModule,
        MatTableModule,
        MatDialogModule,
        MatSnackBarModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        DatabaseListComponent,
        SchemaListComponent,
        TableListComponent,
        ColumnListComponent
      ],
      providers: [NotificationsService, DbService, MessageService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListComponent);
    component = fixture.componentInstance;
    dbService = TestBed.get(DbService);
    mockDbInfo = dbService.currentDB = {
      dbName: 'Database A',
      schemaName: 'Schema A'
    } as DbInfo;
    fixture.detectChanges();
  });

  // it('TableList Component Should Create', () => {
  //   expect(component).toBeTruthy();
  // });

  // it('Table List should be filled', () => {
  //   component.tables = ['Table A', 'Table B', 'Table C', 'Table D'];
  //   expect(component.tables.length).toBeGreaterThan(0);
  // });

  // it('Before next step ,only one table should be selected ', () => {
  //   mockDbInfo.tableName = 'Table A';
  //   expect(mockDbInfo.tableName).toBeDefined();
  // });
});
