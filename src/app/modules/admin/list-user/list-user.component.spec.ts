import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule,
  MatPaginatorModule,
  MatSlideToggleModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ListUserComponent } from './list-user.component';
import { UserService } from 'src/app/services/user/user.service';

describe('ListUserComponent', () => {
  let component: ListUserComponent;
  let fixture: ComponentFixture<ListUserComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCardModule,
        MatGridListModule,
        MatListModule,
        MatDividerModule,
        MatTabsModule,
        MatTableModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatPaginatorModule,
        RouterTestingModule
      ],
      declarations: [ListUserComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUserComponent);
    userService = TestBed.get(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('ListUser component should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should have 5 columns', () => {
    expect(component.displayedColumns.length).toBe(4);
  });
});
