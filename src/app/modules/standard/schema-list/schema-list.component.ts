import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';

import {
  MatSelectionList,
  MatListOption,
  MAT_DIALOG_DATA
} from '@angular/material';
import { anim, shakeAnim } from '../../../animation/animation';
import { DbService } from '../../../services/db/db.service';
import { DialogData } from '../../../services/util/util.service';
import { IndexService } from 'src/app/services/index/index.service';
import { NotificationsService } from 'angular2-notifications';
import { RoutingService } from 'src/app/services/routing/routing.service';

@Component({
  selector: 'app-schema-list',
  templateUrl: './schema-list.component.html',
  styleUrls: ['./schema-list.component.scss'],
  animations: [anim, shakeAnim]
})
export class SchemaListComponent implements OnInit {
  @ViewChild(MatSelectionList)
  schemaleRef: MatSelectionList;

  public visible = false;

  public state = 'state1';

  public shakeState = 'inactive';

  public schemas: string[] = [];

  public selectedOption: MatListOption;

  constructor(
    @Inject(MAT_DIALOG_DATA) private activeService: DialogData,
    private dbService: DbService,
    private routingService: RoutingService,
    public indexService: IndexService,
    public notifyService: NotificationsService

  ) {
    this.animateMe();
  }

  public ngOnInit(): void {
    this.activeService.service
      .getSchemas(this.dbService.currentDB)
      .subscribe(response => {
        this.schemas = response;
      });
  }
  public next(): void {
    if (this.selectedOption) {
      this.dbService.currentDB.schemaName = this.getSelectedValue();
      this.setSchemaName();
      this.visible = !this.visible;
    } else {
      this.shakeState = this.shakeState === 'inactive' ? 'active' : 'inactive';
      this.notifyService.error('Oops !', 'Choose schema from list.');
    }
  }

  public changeSelect($evt): void {
    this.schemaleRef.deselectAll();
    this.schemaleRef.selectedOptions.toggle($evt.option);
    this.selectedOption = $evt.option;
  }

  public animateMe(): void {
    this.state = this.state === 'state1' ? 'state2' : 'state1';
  }

  public setSchemaName(): void {
    this.indexService.currentIndex.databaseMeta.schemaName = this.getSelectedValue();
  }

  public getSelectedValue(): string {
    return this.selectedOption.getLabel().trim();
  }
}
