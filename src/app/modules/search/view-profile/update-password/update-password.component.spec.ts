import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePasswordComponent } from './update-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatFormFieldModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UpdatePasswordComponent', () => {
  let component: UpdatePasswordComponent;
  let fixture: ComponentFixture<UpdatePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [

        MatInputModule,
        MatCardModule,
        MatPaginatorModule,
        MatGridListModule,
        MatListModule,
        MatDividerModule,
        MatTabsModule,
        MatTableModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSnackBarModule, MatFormFieldModule,
        FormsModule, ReactiveFormsModule,
        HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [ UpdatePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
