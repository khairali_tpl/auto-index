import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import {
  MatDialog,
  MatSnackBar,
  MatSnackBarRef,
  SimpleSnackBar,
  MatDialogConfig
} from '@angular/material';

import { MessageDialogComponent } from '../../modules/shared/component/message-dialog/message-dialog.component';
import { ConfirmDialogComponent } from '../../modules/shared/component/confirm-dialog/confirm-dialog.component';
import { DialogComponent } from '../../modules/shared/component/dialog/dialog.component';
import { isArray, isNull, isUndefined } from 'util';

export interface DialogData {
  data: any;
  service: any;
  serviceName: any;
  title: string;
  message: string;
  component?: any;
}

interface ToastConfig {
  duration?: number;
  horizontalPosition?: 'start' | 'end' | 'right' | 'left' | 'center';
  verticalPosition?: 'bottom' | 'top';
}

export function hasValue(data: any): boolean {
  let flag: boolean;
  if (isArray(data)) {
    flag = data != null && data.length > 0;
  } else {
    flag = isNull(data) || isUndefined(data);
  }
  return flag;
}

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  constructor(private dialog: MatDialog, private toast: MatSnackBar) {}

  /**
   *
   * @param message string
   * @param action strig
   * @param config {duration : 2000}
   */
  public showToast(
    message: string,
    action = 'Done',
    config = {} as ToastConfig
  ): MatSnackBarRef<SimpleSnackBar> {
    return this.toast.open(message, action, {
      duration: config.duration ? config.duration : 2000,
      horizontalPosition: config.horizontalPosition
        ? config.horizontalPosition
        : 'right',
      verticalPosition: config.verticalPosition
        ? config.verticalPosition
        : 'top',
      politeness: 'polite'
    });
  }

  /**
   *
   * @param type 'MSG' | 'CONFIGM' | 'CUSTOM'
   * @param config MatDialogConfig
   */
  public createDialog(
    type: 'MSG' | 'CONFIGM' | 'CUSTOM',
    config?: MatDialogConfig
  ): Observable<
    MessageDialogComponent | ConfirmDialogComponent | DialogComponent
  > {
    if (!config) {
      config = { width: '250px', disableClose: false };
    }
    if (type === 'MSG') {
      config.data.component = MessageDialogComponent;
    } else if (type === 'CONFIGM') {
      config.data.component = ConfirmDialogComponent;
    }
    const dlgRef = this.dialog.open(DialogComponent, config);
    return dlgRef.afterClosed();
  }
}
