import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { IndexService } from 'src/app/services/index/index.service';
import { Observable } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';

@Injectable({
  providedIn: 'root'
})
export class IndexResolve implements Resolve<ElasticMeta> {
  constructor(private indexService: IndexService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ElasticMeta> {
    return this.indexService
      .getIndexDetail(route.paramMap.get('indexname'))
      .pipe(
        tap(data => (data.mapping = JSON.parse(data.mapping))),
        tap(data => (data.setting = JSON.parse(data.setting)))
      );
  }
}
