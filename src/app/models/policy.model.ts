export class Policy {
  endpoint_id: number;
  endpoint_name: string;
  policy_id: number;
  policy_name: string;
  policy_description: string;
}
