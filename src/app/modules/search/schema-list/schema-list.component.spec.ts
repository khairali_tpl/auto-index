import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbService } from '../../../services/db/db.service';
import { UtilService } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DatabaseListComponent } from '../database-list/database-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SchemaListComponent } from '../schema-list/schema-list.component';
import { TableListComponent } from '../table-list/table-list.component';
import { ColumnListComponent } from '../column-list/column-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DbInfo } from 'src/app/models/db-info.model';
import { NotificationsService } from 'angular2-notifications';

describe('SchemaListComponent', () => {
  let component: SchemaListComponent;
  let fixture: ComponentFixture<SchemaListComponent>;
  let dbService: DbService;
  let mockDbInfo: DbInfo;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDividerModule,
        MatTableModule,
        MatDialogModule,
        MatSnackBarModule,
        NoopAnimationsModule,
        HttpClientTestingModule
      ],
      declarations: [
        DatabaseListComponent,
        SchemaListComponent,
        TableListComponent,
        ColumnListComponent
      ],
      providers: [NotificationsService, DbService, MessageService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchemaListComponent);
    component = fixture.componentInstance;
    dbService = TestBed.get(DbService);
    mockDbInfo = dbService.currentDB = {
      dbName: 'Database A'
    } as DbInfo;
    fixture.detectChanges();
  });

  // fit('Schame List Component Should Create', () => {
  //   expect(component).toBeTruthy();
  // });

  // it('Schame List should be filled', () => {
  //   component.schemas = ['Schema A', 'Schema B', 'Schema C', 'Schema D'];
  //   expect(component.schemas.length).toBeGreaterThan(0);
  // });

  // it('Before next step ,only one schema should be selected ', () => {
  //   mockDbInfo.schemaName = 'Schema A';
  //   expect(mockDbInfo.schemaName).toBeDefined();
  // });
});
