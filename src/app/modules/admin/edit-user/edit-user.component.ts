import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private router: ActivatedRoute,
    private userService: UserService
  ) {}
  user: User;
  products: any[] = [];
  editForm: FormGroup;
  policy_list: any[] = [];

  ngOnInit() {
    const routeParams = this.router.snapshot.params;

    this.userService.getUserById(routeParams.id).subscribe(data => {
      this.user = data.user;
      this.editForm = this.formBuilder.group({
        user_id: [this.user.user_id],
        username: [this.user.username, Validators.required],
        email: [this.user.email, Validators.required],
        password: ['', Validators.required],
        company: [this.user.company, Validators.required],
        phone: [this.user.phone, Validators.required],
        address: [this.user.address, Validators.required],
        designation: [this.user.designation, Validators.required],
        purpose_of_usage: [this.user.purpose_of_usage, Validators.required],
        policy_list: new FormControl([])
      });

      for (let i = 0; i < Object.keys(data.accessTypes).length; i++) {
        const policy_id = data.accessTypes[i].policy_id.split(',');
        const policy_name = data.accessTypes[i].policy_name.split(',');
        const endpoint_id = data.accessTypes[i].endpoint_id.split(',');
        const endpoint_name = data.accessTypes[i].endpoint_name.split(',');

        const policies = [];
        for (let j = 0; j < policy_id.length; j++) {
          if (data.selectedAccessTypes.length > 0) {
            const selected_policy = data.selectedAccessTypes[i].policy_id.split(
              ','
            );
            if (selected_policy.indexOf(policy_id[j]) !== -1) {
              policies.push({
                policy_id: policy_id[j],
                policy_name: policy_name[j],
                selected: true
              });
              this.policy_list.push(policy_id[j]);
            } else {
              policies.push({
                policy_id: policy_id[j],
                policy_name: policy_name[j],
                selected: false
              });
            }
          } else {
            policies.push({
              policy_id: policy_id[j],
              policy_name: policy_name[j],
              selected: false
            });
          }
        }
        this.products.push({
          product_id: data.accessTypes[i].product_id,
          product_name: data.accessTypes[i].product_name,
          policies: policies
        });
      }
    });
  }

  onSubmit() {
    this.editForm.controls.policy_list.setValue(this.policy_list);
    this.userService.updateUser(this.editForm.value).subscribe(data => {});
  }

  onSelection(e, v) {
    if (e.option.selected) {
      this.policy_list.push(e.option.value);
    } else {
      if (this.policy_list.indexOf(e.option.value) !== -1) {
        this.policy_list.splice(this.policy_list.indexOf(e.option.value), 1);
      }
    }
  }
}
