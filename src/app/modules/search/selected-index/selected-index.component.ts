import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IndexService } from 'src/app/services/index/index.service';
import { map } from 'rxjs/operators';

interface Link {
  name: string;
  url: string;
}

@Component({
  selector: 'app-selected-index',
  templateUrl: './selected-index.component.html',
  styleUrls: ['./selected-index.component.scss']
})
export class SelectedIndexComponent {
  public links: Link[] = [];

  public activeLink = undefined;

  constructor(
    private activeRoute: ActivatedRoute,
    private route: Router,
    private indexService: IndexService
  ) {
    this.activeRoute.data.pipe(map(data => data['data'])).subscribe(meta => {
      this.indexService.currentIndex = meta;
    });

    this.activeRoute.params.subscribe(response => {
      // this.links = this.generatePath(response.indexname);
      this.activeLink = this.links[0];
    });
  }

  public navigateTo(link: Link): void {
    this.activeLink = link;
    this.route.navigate([link.url]);
  }

  public getCurrentIndex(): string {
    return this.indexService.currentIndex.indexName;
  }

  private generatePath(indexName: string): any[] {
    return [
      {
        name: 'Data',
        url: `app/selected-index/${indexName}/data`
      },
      {
        name: `Ranking`,
        url: `app/selected-index/${indexName}/ranking`
      },
      {
        name: `Synonyms`,
        url: `app/selected-index/${indexName}/synonyms`
      }
    ];
  }
}
