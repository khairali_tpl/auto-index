import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from '../util/util.service';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class InterceptorService {
  constructor(public router: Router, public utilService: UtilService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        Accept: 'application/json',
        Authorization: this.createAuthKey(req)
      }
    });
    return next.handle(req).pipe(
      tap(
        data => {},
        (error: HttpErrorResponse) => {
          if (!req.headers.has('Custom-Error')) {
            if (error.status === 401) {
              document.location.replace('???');
            } else {
              this.digestError(error);
            }
          }
        }
      )
    );
  }

  private applyCredential(req: HttpRequest<any>): boolean {
    req = req.clone();
    return !(
      req.headers['headers'].get('skip_credential') &&
      req.headers['headers'].get('skip_credential')[0]
    );
  }
  private createAuthKey(req: HttpRequest<any>): string {
    return req.params.get('id')
      ? 'Basic ' + btoa(req.params.get('id') + ':' + req.params.get('password'))
      : `Bearer ${sessionStorage.getItem('access_token')}`;
  }

  private digestError(errRes: HttpErrorResponse): void {
    let title = '';
    let message = '';
    if (
      errRes.message.toLowerCase().includes('unknown') &&
      errRes.status == 0
    ) {
      (title = 'Error'), (message = 'Services are down temporarily');
    } else {
      title = 'LBS Error';
      message = errRes.error.message;
    }
    this.utilService.createDialog('MSG', { data: { title, message } });
  }
}
