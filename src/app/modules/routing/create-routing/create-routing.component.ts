import { Component, OnInit, Input } from '@angular/core';
import { UtilService } from 'src/app/services/util/util.service';
import { DbConfigComponent } from '../../standard/db-config/db-config.component';
import { DbService } from 'src/app/services/db/db.service';
import { FormGroup, FormControl } from '@angular/forms';
import { RoutingService } from '../service/routing.service';
import { RoutingConfigComponent } from '../../standard/routing-config/routing-config.component';
import { DbInfo } from 'src/app/models/db-info.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-routing',
  templateUrl: './create-routing.component.html',
  styleUrls: ['./create-routing.component.scss']
})
export class CreateRoutingComponent implements OnInit {
  public table_columns: string[] = [];
  public routingForm: FormGroup;

  constructor(
    private utilService: UtilService,
    private dbService: DbService,
    private routingService: RoutingService
  ) {
    this.dbService.currentDB = {} as DbInfo;
  }

  ngOnInit() {
    this.routingForm = new FormGroup({
      restrictedColumnName: new FormControl(''),
      priorityColumnName: new FormControl(''),
      usageColumnName: new FormControl(''),
      speedColumnName: new FormControl(''),
      directionColumnName: new FormControl(''),
      nameColumn: new FormControl(''),
      roundaboutIdentification: new FormControl(''),
      uturnIdentification: new FormControl(''),
      directionIdentification: new FormControl(''),
      restrictedIdentification: new FormControl(''),
      semiRestrictedIdentification: new FormControl('')
    });
    const config = {
      width: '500px',
      disableClose: false,
      data: {
        component: DbConfigComponent,
        service: this.routingService,
        serviceName: 'RoutingService'
      }
    };

    this.utilService.createDialog('CUSTOM', config).subscribe(data => {
      this.routingService
        .getcolumns(this.dbService.currentDB)
        .subscribe(response => {
          this.table_columns = response;
        });
    });
  }

  saveGraphConfig() {
    this.routingService
      .generateGraph(this.routingForm.value)
      .subscribe(data => {});
  }

  public publishService() {
    this.routingService.publishService().subscribe(response => {
      this.utilService.showToast(response, 'DONE');
    });
  }
}
