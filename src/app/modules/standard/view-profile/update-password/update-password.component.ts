import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordValidation } from './password-validator';
import { UserService } from 'src/app/services/user/user.service';
import { UtilService } from 'src/app/services/util/util.service';
import { MessageService } from 'src/app/services/message/message.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  name = 'Angular 6';
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private notifyService: NotificationsService,
    private msgService: MessageService
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group(
      {
        oldpassword: ['', Validators.required],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      },
      {
        validator: PasswordValidation.MatchPassword
      }
    );
  }

  public onSubmitMe(): void {
    this.userService
      .updatePassword(this.form.value.oldpassword, this.form.value.password)
      .subscribe(response => {

        this.notifyService.success('Hurray !', 'Password updated successfully.');
        this.msgService.sendDlgMsg('CLOSE');
      });
  }
}
