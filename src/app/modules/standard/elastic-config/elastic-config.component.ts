import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnInit,
  Inject
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilService, DialogData } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DbService } from '../../../services/db/db.service';
import { interval, pipe } from 'rxjs';
import { shakeAnim, shakeMe } from 'src/app/animation/animation';
import { take } from 'rxjs/operators';
import { IndexService } from 'src/app/services/index/index.service';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-elastic-config',
  templateUrl: './elastic-config.component.html',
  styleUrls: ['./elastic-config.component.scss'],
  animations: [shakeAnim]
})
export class ElasticConfigComponent implements OnInit {
  public form: FormGroup;

  public shakeState = 'inactive';

  public visible = false;

  public databaseList = [];

  @Input()
  public dlgReference: any;

  constructor(
    private spinnerService: SpinnerService,
    private notifyService: NotificationsService,
    private msgService: MessageService,
    private indexService: IndexService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  public connectElastic(): void {
    this.spinnerService.requestInProcess(true);
    this.indexService.getIndicies(this.form.value).subscribe(
      resp => {
        this.spinnerService.requestInProcess(false);
        this.msgService.sendDlgMsg('CLOSE', resp);
      },
      error => {
        this.shakeState = shakeMe(this.shakeState);
        this.spinnerService.requestInProcess(false);
        this.notifyService.error('Oops !', 'Invalid credentials.');
      }
    );
  }

  public closeDlg(): void {
    this.msgService.sendDlgMsg('CLOSE');
  }

  private buildForm(): void {
    this.form = new FormGroup({
      host: new FormControl('localhost', Validators.required),
      port: new FormControl(9300, Validators.required),
      username: new FormControl('postgres', Validators.required),
      password: new FormControl('postgres', Validators.required),
      clusterName: new FormControl('postgres', Validators.required)
    });
  }
}
