import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRoutingComponent } from './create-routing.component';
import {
  MatCardModule,
  MatOptionModule,
  MatSelectModule,
  MatGridListModule,
  MatDividerModule,
  MatSidenavModule,
  MatDialogModule,
  MatSnackBarModule,
  MatListModule,
  MatProgressBarModule,
  MatDialogConfig,
  MatDialog,
  MatInputModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ColumnListComponent } from '../../standard/column-list/column-list.component';
import { HttpClient } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingService } from '../service/routing.service';
import { DbService } from 'src/app/services/db/db.service';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { DialogComponent } from '../../shared/component/dialog/dialog.component';
import { Observable } from 'rxjs';
import { UtilService } from 'src/app/services/util/util.service';
import { UserService } from 'src/app/services/user/user.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

class UtilServiceMock {
  public createDialog(
    type: 'MSG' | 'CONFIGM' | 'CUSTOM',
    config?: MatDialogConfig
  ): Observable<any> {
    return {} as Observable<any>;
  }
}

@NgModule({
  imports: [
        CommonModule,
        MatCardModule,
        MatProgressBarModule,
  ],
  declarations: [DialogComponent],
  entryComponents: [DialogComponent]
})
export class FakeTestDialogModule {}

describe('CreateRoutingComponent', () => {
  let component: CreateRoutingComponent;
  let fixture: ComponentFixture<CreateRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MatCardModule,
        MatOptionModule,
        MatSelectModule,
        MatGridListModule,
        MatDividerModule,
        MatSidenavModule,
        MatDialogModule,
        MatSnackBarModule,
        MatProgressBarModule,
        RouterTestingModule,
        MatListModule,
        NoopAnimationsModule,
        FakeTestDialogModule,
        MatInputModule
      ],
      declarations: [
        CreateRoutingComponent,
        ColumnListComponent,
      ],
      providers: [
        { provide: HttpClient, useValue: {} },
        {
          provide: UtilServiceMock
        },
        UserService,
        RoutingService,
        DbService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
