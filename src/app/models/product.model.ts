import { Policy } from './policy.model';

export class Product {
  product_id: number;
  product_name: string;
  policy: Policy[];
}
