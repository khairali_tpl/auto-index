import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDemoComponent } from './search-demo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IndexService } from '../services/index/index.service';
import { LoadingService } from '../services/spinner/loading.service';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import {
  MatCardModule,
  MatInputModule,
  MatProgressBarModule
} from '@angular/material';
import { LoadingComponent } from '../modules/shared/component/loading/loading.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('SearchDemoComponent', () => {
  let component: SearchDemoComponent;
  let fixture: ComponentFixture<SearchDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NgxJsonViewerModule,
        MatCardModule,
        MatInputModule,
        MatProgressBarModule,
        HttpClientTestingModule,
        NoopAnimationsModule
      ],
      declarations: [SearchDemoComponent, LoadingComponent],
      providers: [IndexService, LoadingService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('demo component should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have search bar', () => {
    const result = fixture.nativeElement.querySelector('input');
    expect(result).toBeTruthy();
  });

  it('there should be  json viewer', () => {
    fixture.componentInstance.showResult = true;
    fixture.componentInstance.elasticData = true;
    fixture.detectChanges();
    const result = fixture.nativeElement.querySelector('ngx-json-viewer');
    expect(result).toBeTruthy();
  });
});
