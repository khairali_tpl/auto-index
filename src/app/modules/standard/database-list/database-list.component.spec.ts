import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbService } from '../../../services/db/db.service';
import { UtilService } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DatabaseListComponent } from '../database-list/database-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SchemaListComponent } from '../schema-list/schema-list.component';
import { TableListComponent } from '../table-list/table-list.component';
import { ColumnListComponent } from '../column-list/column-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DbInfo } from 'src/app/models/db-info.model';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';

describe('DatabaseListComponent', () => {
  let component: DatabaseListComponent;
  let fixture: ComponentFixture<DatabaseListComponent>;
  let dbService: DbService;
  let mockDbInfo: DbInfo;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDividerModule,
        MatTableModule,
        MatDialogModule,
        MatSnackBarModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        SimpleNotificationsModule.forRoot()
      ],
      declarations: [
        DatabaseListComponent,
        SchemaListComponent,
        TableListComponent,
        ColumnListComponent
      ],
      providers: [NotificationsService, DbService, MessageService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatabaseListComponent);
    dbService = TestBed.get(DbService);
    component = fixture.componentInstance;
    mockDbInfo = dbService.currentDB = {} as DbInfo;

    fixture.detectChanges();
  });

  it('Database List Component Should Create', () => {
    expect(component).toBeTruthy();
  });

  it('Database List should be filled', () => {
    component.databaseList = [
      'Database A',
      'Database B',
      'Database C',
      'Database D'
    ];
    expect(component.databaseList.length).toBeGreaterThan(0);
  });

  it('Before next step ,only one  database name should be selected ', done => {
    mockDbInfo.dbName = 'Database A';
    expect(mockDbInfo.dbName).toBeDefined();
    done();
  });
});
