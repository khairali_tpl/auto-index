import { MediaMatcher } from '@angular/cdk/layout';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
  Route,
  ChildActivationStart
} from '@angular/router';
import { NavItem } from 'src/app/models/nav-item.model';
import { Navigation } from 'src/app/models/navigation.model';
import { NavigationService } from 'src/app/modules/shared/navigation/navigation.service';
import { take, filter } from 'rxjs/operators';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements AfterViewInit {
  @ViewChild('appDrawer') appDrawer: ElementRef;
  navItems: NavItem[];
  title = 'my-app';

  constructor(private navService: NavigationService, private route: Router) {
    this.route.events
      .pipe(
        filter(data => data instanceof ChildActivationStart),
        take(1)
      )

      .subscribe(r => {});
    this.navItems = Navigation.navItems;
  }

  ngAfterViewInit() {
    this.navService.appDrawer = this.appDrawer;
  }
}
