import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingConfigComponent } from './routing-config.component';

import { DbService } from '../../../services/db/db.service';
import { UtilService } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DatabaseListComponent } from '../database-list/database-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SchemaListComponent } from '../schema-list/schema-list.component';
import { TableListComponent } from '../table-list/table-list.component';
import { ColumnListComponent } from '../column-list/column-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingService } from '../../routing/service/routing.service';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';

describe('RoutingConfigComponent', () => {
  let component: RoutingConfigComponent;
  let fixture: ComponentFixture<RoutingConfigComponent>;
  let routingService: RoutingService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDividerModule,
        MatTableModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        SimpleNotificationsModule.forRoot()
      ],
      declarations: [
        RoutingConfigComponent,
        DatabaseListComponent,
        SchemaListComponent,
        TableListComponent,
        ColumnListComponent
      ],
      providers: [NotificationsService, DbService, MessageService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingConfigComponent);
    component = fixture.componentInstance;
    routingService = TestBed.get(DbService);
    fixture.detectChanges();
  });

  it('RoutingConfigComponent should create', () => {
    expect(component).toBeTruthy();
  });

  it('Form invalid when empty', () => {
    expect(component.form.valid).toBeTruthy();
  });

  it('Database Name form field should exist', done => {
    const dbName = component.form.controls['dbName'];
    expect(dbName).toBeDefined();
    done();
  });

  it('Port form field should exist', () => {
    const port = component.form.controls['port'];
    expect(port).toBeDefined();
  });

  it('Host/IP form field should exist', () => {
    const ip = component.form.controls['ip'];
    expect(ip).toBeDefined();
  });

  it('User Name form field should exist', () => {
    const userName = component.form.controls['userName'];
    expect(userName).toBeDefined();
  });

  it('Password form field should exist', () => {
    const password = component.form.controls['password'];
    expect(password).toBeDefined();
  });

  it('Form should be validate before service call', () => {
    component.form.setValue({
      dbName: 'any',
      ip: 'localhost',
      port: 5432,
      userName: 'postgres',
      password: 'postgres'
    });
    expect(component.form.valid).toBeTruthy();
  });

  // Backend services are not running
  // it('Connect to DB', () => {
  //   spyOn(routingService, 'getDatabases').and.callThrough();
  //   component.connectDB();
  //   expect(routingService.getDatabases).toHaveBeenCalled();
  // });
});
