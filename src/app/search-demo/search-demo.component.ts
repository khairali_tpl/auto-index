import { Component, OnInit } from '@angular/core';
import { IndexService } from '../services/index/index.service';
import { Observable } from 'rxjs';
import {
  mergeMap,
  map,
  toArray,
  debounce,
  debounceTime,
  tap
} from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SpinnerService } from '../services/spinner/spinner.service';

@Component({
  selector: 'app-search-demo',
  templateUrl: './search-demo.component.html',
  styleUrls: ['./search-demo.component.scss']
})
export class SearchDemoComponent {
  public form: FormGroup;

  public showResult = false;

  public elasticData: any;

  constructor(
    private fb: FormBuilder,
    private indexService: IndexService,
    private spinnerService: SpinnerService
  ) {
    this.buildForm();
  }

  public buildForm(): void {
    this.form = this.fb.group({
      searchValue: new FormControl(undefined)
    });

    this.form.controls.searchValue.valueChanges
      .pipe(
        debounceTime(2000),
        tap(data => (this.elasticData = undefined)),
        tap(data => this.spinnerService.requestInProcess(true)),
      )
      .subscribe(resposne => {
        this.loadData(resposne);
        this.showResult = true;
      });
  }

  private loadData(val: string): void {
    this.indexService.searchData(val).subscribe(resposne => {
      this.elasticData = resposne;
      this.spinnerService.requestInProcess(false);
    });
  }
}
