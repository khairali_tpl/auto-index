import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';

import { StandardRoutingModule } from './standard-routing.module';
// import { BaseComponent } from './base/base.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { IndiciesComponent } from './indicies/indicies.component';
import { SelectedIndexComponent } from './selected-index/selected-index.component';
import { IndexDataComponent } from './index-data/index-data.component';
import { RankingComponent } from './ranking/ranking.component';
import { SynonymsComponent } from './synonyms/synonyms.component';
import { SynonymsDialogComponent } from './synonyms/synonyms-dialog/synonyms-dialog.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { SatPopoverModule } from '@ncstate/sat-popover';
import { EditComponent } from './view-profile/edit/edit.component';
import { UpdatePasswordComponent } from './view-profile/update-password/update-password.component';
import { BaseComponent } from './base/base.component';


@NgModule({
  imports: [CommonModule, StandardRoutingModule, LayoutModule, SharedModule, NgxJsonViewerModule, SatPopoverModule],
  declarations: [
    HomeComponent,
    IndiciesComponent,
    SelectedIndexComponent,
    IndexDataComponent,
    RankingComponent,
    SynonymsComponent,
    SynonymsDialogComponent,
    ViewProfileComponent,
    EditComponent
  ],
  entryComponents: [SynonymsDialogComponent, SynonymsComponent]
})
export class StandardModule {
  constructor() {}
}
