import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class BaseService {
  protected url = 'http://backend.tplmaps.com:8888';
  // protected url = 'http://localhost:8888';

  protected endpoint = '';

  constructor(protected http: HttpClient) {}

  /**
   *
   * @param action string
   * @param params {}
   */
  protected get(action: string, params?: {}): Observable<any> {
    return this.http.get(this.getRestURL(action), {
      params
    });
  }

  /**
   *
   * @param action string
   * @param data {}
   */
  protected post(action: string, data?: any, params?: any): Observable<any> {
    return this.http.post(this.getRestURL(action), data, {
      params
    });
  }

  /**
   *
   * @param action string
   * @param data {}
   */
  protected put(action: string, data?: any, params?: any): Observable<any> {
    return this.http.put(this.getRestURL(action), data, {
      params
    });
  }

  private getRestURL(action: string): string {
    console.log(this.url, action, this.endpoint);
    return this.endpoint.length > 0
      ? `${this.url}/${this.endpoint}/${action}`
      : `${this.url}/${action}`;
  }
}
