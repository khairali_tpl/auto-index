import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopToolBarComponent } from './top-tool-bar.component';
import { NavigationService } from '../navigation.service';
import { RouterTestingModule } from '@angular/router/testing';
import { MatIconModule, MatToolbarModule, MatMenuModule } from '@angular/material';

describe('TopToolBarComponent', () => {
  let component: TopToolBarComponent;
  let fixture: ComponentFixture<TopToolBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatIconModule, MatToolbarModule, MatMenuModule],
      declarations: [TopToolBarComponent],
      providers: [NavigationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopToolBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
