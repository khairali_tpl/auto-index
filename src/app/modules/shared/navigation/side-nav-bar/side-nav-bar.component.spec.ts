import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NavigationService } from 'src/app/modules/shared/navigation/navigation.service';
import { SideNavBarComponent } from './side-nav-bar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared.module';
import { NavItem } from 'src/app/models/nav-item.model';
import { MatIconModule } from '@angular/material';

describe('SideNavBarComponent', () => {
  let component: SideNavBarComponent;
  let fixture: ComponentFixture<SideNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatIconModule],
      declarations: [SideNavBarComponent],
      // schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [NavigationService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavBarComponent);
    component = fixture.componentInstance;
    component.item = {} as NavItem;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
