import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  // Observable string sources
  requestInProcessSource = new Subject<boolean>();
  // Observable string streams
  requestInProcess$ = this.requestInProcessSource.asObservable();
  // Service message commands
  requestInProcess(isRequesting: boolean) {
    setTimeout(() => {
      this.requestInProcessSource.next(isRequesting);
    }, 0);
  }
}
