import { Component, OnInit, Inject } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatChipInputEvent
} from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-synonyms-dialog',
  templateUrl: './synonyms-dialog.component.html',
  styleUrls: ['./synonyms-dialog.component.scss']
})
export class SynonymsDialogComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  synonyms: any[] = [];
  allSynonyms: any[] = [];
  addForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<SynonymsDialogComponent>
  ) {}

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      synonym: ['']
    });
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add synonym word
    if ((value || '').trim()) {
      this.synonyms.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(element: any): void {
    const index = this.synonyms.indexOf(element);

    if (index >= 0) {
      this.synonyms.splice(index, 1);
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  saveLocal(): void {
    this.allSynonyms.push(this.synonyms.toString());
    this.synonyms = [];
  }

  saveSynonyms(): void {
    this.dialogRef.close(this.allSynonyms);
  }
}
