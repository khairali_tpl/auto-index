import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { MessageService } from '../../../services/message/message.service';
import { Router } from '@angular/router';
import {
  MatSelectionList,
  MatListOption,
  MAT_DIALOG_DATA
} from '@angular/material';
import { anim, shakeAnim } from '../../../animation/animation';
import { DbService } from '../../../services/db/db.service';
import { IndexInfo } from '../../../models/index-info.model';
import { IndexService } from '../../../services/index/index.service';
import { UtilService, DialogData } from '../../../services/util/util.service';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { RoutingService } from 'src/app/services/routing/routing.service';
import { LoadingService } from 'src/app/services/spinner/loading.service';

@Component({
  selector: 'app-column-list',
  templateUrl: './column-list.component.html',
  styleUrls: ['./column-list.component.scss'],
  animations: [anim, shakeAnim]
})
export class ColumnListComponent implements OnInit {
  public state = 'state1';

  public shakeState = 'inactive';

  public columns: string[] = [];

  public readonly meta: ElasticMeta;

  public indexMapping = {
    mappings: {
      data: {
        properties: {}
      }
    },
    settings: {}
  };

  @ViewChild(MatSelectionList)
  tableRef: MatSelectionList;

  constructor(
    @Inject(MAT_DIALOG_DATA) private activeService: DialogData,
    private route: Router,
    private msgService: MessageService,
    private dbService: DbService,
    private routingService: RoutingService,
    private indexService: IndexService,
    private utilService: UtilService,
    private loadingService: LoadingService
  ) {
    this.animateMe();
    this.meta = this.indexService.currentIndex;
  }

  ngOnInit(): void {
    this.activeService.service
      .getcolumns(this.meta.databaseMeta)
      .subscribe(response => {
        this.columns = response;
      });
  }

  public createIndex(): void {
    if (this.getSelectedOptions().length > 0) {
      this.loadingService.start('PROGRESSDLG');
      this.meta.mapping = this.getIndexMapping();
      this.meta.databaseMeta.columns = this.getSelectedColumns();
      this.meta.indexName = this.tableName;
      this.meta.setting = this.getSettingSkeleton();
      console.log(this.meta);
      this.indexService.createIndex(true, this.meta).subscribe(
        response => {
          this.utilService.showToast('Index Created', 'Done');
          this.route.navigate([`app/selected-index/${this.tableName}`]);
          this.msgService.sendDlgMsg('CLOSE');
          this.loadingService.complete();
        },
        error => {
          this.loadingService.complete();
        }
      );
    } else {
      this.shakeState = this.shakeState === 'inactive' ? 'active' : 'inactive';
      this.utilService.showToast('Choose Column', 'Error', {
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
    }
  }

  public get tableName(): string {
    return this.meta.databaseMeta.tableName;
  }

  public animateMe(): void {
    this.state = this.state === 'state1' ? 'state2' : 'state1';
  }

  public getSelectedOptions(): MatListOption[] {
    return this.tableRef.selectedOptions.selected;
  }

  /**
   * will apply closure
   */

  private getIndexMapping(): string {
    this.getSelectedOptions().forEach(data => {
      this.setMappingValue(data.getLabel().trim());
    });
    return JSON.stringify(this.indexMapping);
  }

  private getSelectedColumns(): string[] {
    return this.getSelectedOptions().map(data => data.getLabel().trim());
  }

  private setMappingValue(colName: string): void {
    this.indexMapping.mappings.data.properties[colName] = {
      type: 'text',
      enabled: false
    };
  }

  private getSettingSkeleton(): any {
    const obj = {
      settings: {
        index: {
          analysis: {
            analyzer: {
              synonym: {
                tokenizer: 'standard',
                filter: []
              }
            },
            filter: {}
          }
        }
      }
    };
    return JSON.stringify(obj);
  }
}
