import { Injectable, EventEmitter } from '@angular/core';

export type LoadingType = 'PROGRESS' | 'LOADING' | 'COMPLETE' | 'PROGRESSDLG';
@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  public _loadingEmitter$ = new EventEmitter<LoadingType>();
  constructor() {}

  public start(type: 'PROGRESS' | 'LOADING' | 'PROGRESSDLG' = 'LOADING'): void {
    this._loadingEmitter$.next(type);
  }

  public complete(): void {
    this._loadingEmitter$.next('COMPLETE');
  }

  public get loadingEmitter$(): EventEmitter<LoadingType> {
    return this._loadingEmitter$;
  }
}
