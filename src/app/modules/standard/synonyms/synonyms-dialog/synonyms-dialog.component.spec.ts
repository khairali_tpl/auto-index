import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynonymsDialogComponent } from './synonyms-dialog.component';
import { MatChipsModule, MatIconModule, MatFormFieldModule, MatDialogRef, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SynonymsDialogComponent', () => {
  let component: SynonymsDialogComponent;
  let fixture: ComponentFixture<SynonymsDialogComponent>;
  const fakeActivatedRoute = {
    snapshot: { data: {} }
  } as ActivatedRoute;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatChipsModule,
        MatIconModule,
        MatFormFieldModule,
        MatDialogModule,
        NoopAnimationsModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [SynonymsDialogComponent],
      providers: [{ provide: MatDialogRef, useValue: {} }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have dialog label', () => {
    const el = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(el.innerHTML).toEqual('Add Expansion');
  });

  it('should have input field and perform correctly by click on button', () => {
    const btn = fixture.nativeElement.querySelector('#local-btn');
    const input = fixture.debugElement.query(By.css('input'));
    const el = input.nativeElement;
    el.value = 'someValue';
    el.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    btn.click();
    fixture.detectChanges();
    const chipEle = fixture.nativeElement.querySelector('.mat-standard-chip');
    expect(chipEle).toBeTruthy();
  });

  it('dialog should have save and cancel button', () => {
    const saveBtn = fixture.nativeElement.querySelector('#save-btn');
    const cancelBtn = fixture.nativeElement.querySelector('#cncl-btn');
    expect(saveBtn).toBeTruthy();
    expect(cancelBtn).toBeTruthy();
  });
});
