import { Injectable, EventEmitter } from '@angular/core';

interface EmitterObj {
  status: 'CLOSE' | 'OK';
  data: any;
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  public _eventEmitter$ = new EventEmitter<EmitterObj>();

  constructor() {}

  public sendDlgMsg(status: 'CLOSE' | 'OK', data?: any): void {
    this._eventEmitter$.next({ status, data });
  }

  public get eventEmitter$(): EventEmitter<EmitterObj> {
    return this._eventEmitter$;
  }
}
