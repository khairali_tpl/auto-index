import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../navigation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-tool-bar',
  templateUrl: './top-tool-bar.component.html',
  styleUrls: ['./top-tool-bar.component.scss']
})
export class TopToolBarComponent implements OnInit {

  constructor(public navService: NavigationService, private route: Router
  ) { }

  ngOnInit() {
  }

  viewProfile() {
    this.route.navigate(['app/view-profile']);
  }
  logout() {
    // this.cookieService.set( 'current_user', JSON.stringify(response) );
    // this.cookieService.delete('current_user');
    // this.route.navigate(['app']);
  }

}
