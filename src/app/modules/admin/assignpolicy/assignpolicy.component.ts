import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/models/role.model';

@Component({
  selector: 'app-assignpolicy',
  templateUrl: './assignpolicy.component.html',
  styleUrls: ['./assignpolicy.component.scss']
})
export class AssignpolicyComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  users: User[];
  roles: Role[];

  selected_user: number;
  assignForm: FormGroup;
  policy_list: any[] = [];
  products: any[] = [];

  ngOnInit() {
    this.assignForm = this.formBuilder.group({
      role_id: ['', Validators.required],
      policy_list: new FormControl([])
    });

    this.userService.getAccessTypes().subscribe(data => {
      for (let i = 0; i < Object.keys(data).length; i++) {
        const policy_id = data[i].policy_id.split(',');
        const policy_name = data[i].policy_name.split(',');
        const endpoint_id = data[i].endpoint_id.split(',');
        const endpoint_name = data[i].endpoint_name.split(',');

        const policies = [];
        for (let j = 0; j < policy_id.length; j++) {
          policies.push({
            policy_id: policy_id[j],
            policy_name: policy_name[j]
          });
        }
        this.products.push({
          product_id: data[i].product_id,
          product_name: data[i].product_name,
          policies: policies
        });
      }
    });

    this.userService.getAllRoles().subscribe(
      data => {
        this.roles = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  onSubmit() {
    /*this.assignForm.controls.policy_list.setValue(this.policy_list);
    console.log(this.assignForm.value);
    this.userService.createUser(this.assignForm.value)
    .subscribe( data => {

    });*/
    console.log(this.assignForm.controls.role_id.value);
  }
}
