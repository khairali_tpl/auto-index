import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  constructor(private router: Router, private userService: UserService) {}

  users: User[];
  displayedColumns: string[] = [
    'userName',
    'emailAddress',
    'status',
    'actions'
  ];
  dataSource;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  ngOnInit() {
    this.userService.getUsers().subscribe(
      data => {
        this.users = data;
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.dataSource.paginator = this.paginator;
      },
      error => {
      }
    );
  }

  disableUser(value, element) {
    element.is_enable = value ? true : false;
    if (element.is_enable) {
      this.userService.enableUser(element.user_id).subscribe(data => {});
    } else {
      this.userService.disableUser(element.user_id).subscribe(data => {});
    }
  }
}
