import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingbaseComponent } from './routingbase/routingbase.component';
import { CreateRoutingComponent } from './create-routing/create-routing.component';
import { BaseComponent } from '../standard/base/base.component';
const data = { type: 'ROUTING' };
const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    // data,
    children: [
      { path: '', redirectTo: 'create-routing' },
      { path: 'create-routing', component: CreateRoutingComponent }
    ]
  }
];

export const RoutingRoutingModule = RouterModule.forChild(routes);
