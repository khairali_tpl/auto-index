import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from 'src/app/services/base/base.service';
import { Observable } from 'rxjs';
import { GenerateGraph } from '../models/generate.graph.model';
import { DbInfo } from 'src/app/models/db-info.model';

@Injectable({
  providedIn: 'root'
})
export class RoutingService extends BaseService {
  private _currentDB: DbInfo;

  constructor(protected http: HttpClient) {
    super(http);
    this.endpoint = 'routing/dbmanager';
  }

  public get currentDB(): DbInfo {
    return this._currentDB;
  }

  public set currentDB(data: DbInfo) {
    this._currentDB = data;
  }

  public getDatabases(dbInfo: DbInfo): Observable<DbInfo[]> {
    return this.post('getconnection', dbInfo);
  }

  public getSchemas(dbInfo: DbInfo): Observable<string[]> {
    return this.get('getschemas', {
      dbName: dbInfo.dbName,
      userName: dbInfo.userName,
      password: dbInfo.password
    });
  }

  public getTables(schemaName: String): Observable<string[]> {
    return this.get('gettables', { schemaName });
  }

  public getcolumns(dbInfo: DbInfo): Observable<string[]> {
    const schemaName = dbInfo.schemaName;
    const tableName = dbInfo.tableName;
    return this.get('getcolumns', { schemaName, tableName });
  }

  public generateGraph(graph: GenerateGraph): Observable<any> {
    return this.post('graphconfig', graph);
  }

  public publishService(): Observable<any> {
    return this.get('creategraph');
  }
}
