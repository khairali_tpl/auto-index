import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilService } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DbService } from '../../../services/db/db.service';
import { RoutingService } from '../../routing/service/routing.service';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';
import { NotificationsService } from 'angular2-notifications';



@Component({
  selector: 'app-routing-config',
  templateUrl: './routing-config.component.html',
  styleUrls: ['./routing-config.component.scss']
})
export class RoutingConfigComponent implements OnInit {

  public form: FormGroup;

  public visible = false;

  public databaseList = [];

  @Input()
  public dlgReference: any;

  constructor(
    private notifyService: NotificationsService,
    private spinnerService: SpinnerService,
    private msgService: MessageService,
    private routingService: RoutingService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  public connectDB(): void {
    this.spinnerService.requestInProcess(true);
    this.routingService.getDatabases(this.form.value).subscribe(
      response => {
        this.routingService.currentDB = this.form.value;
        this.visible = !this.visible;
        this.databaseList = response;
        this.spinnerService.requestInProcess(false);
      },
      error => {
        this.spinnerService.requestInProcess(false);
        this.notifyService.error('Oops !', 'Invalid information.');
      }
    );
  }

  public closeDlg(): void {
    this.msgService.sendDlgMsg('CLOSE');
  }

  private buildForm(): void {
    this.form = new FormGroup({
      dbName: new FormControl('farsi'),
      ip: new FormControl('localhost', Validators.required),
      port: new FormControl(5432, Validators.required),
      userName: new FormControl('postgres', Validators.required),
      password: new FormControl('postgres', Validators.required)
    });
  }

}
