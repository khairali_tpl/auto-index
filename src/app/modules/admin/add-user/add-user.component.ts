import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  addForm: FormGroup;
  products: any[] = [];
  displayedColumns: string[] = [];
  policy_list: any[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      company: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      designation: ['', Validators.required],
      purpose_of_usage: ['', Validators.required],
      policy_list: new FormControl([])
    });

    this.userService.getAccessTypes().subscribe(data => {
      for (let i = 0; i < Object.keys(data).length; i++) {
        const policy_id = data[i].policy_id.split(',');
        const policy_name = data[i].policy_name.split(',');
        const endpoint_id = data[i].endpoint_id.split(',');
        const endpoint_name = data[i].endpoint_name.split(',');

        const policies = [];
        for (let j = 0; j < policy_id.length; j++) {
          policies.push({
            policy_id: policy_id[j],
            policy_name: policy_name[j]
          });
        }
        this.products.push({
          product_id: data[i].product_id,
          product_name: data[i].product_name,
          policies: policies
        });
      }
    });
  }

  onSubmit() {
    this.addForm.controls.policy_list.setValue(this.policy_list);
    this.userService.createUser(this.addForm.value).subscribe(data => {});
  }

  onSelection(e, v) {
    this.policy_list.push(e.option.value);
  }
}
