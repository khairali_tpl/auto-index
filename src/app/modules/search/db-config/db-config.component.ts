import { Component, Output, EventEmitter, Input, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilService, DialogData } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DbService } from '../../../services/db/db.service';
import { IndexService } from 'src/app/services/index/index.service';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { MAT_DIALOG_DATA } from '@angular/material';
import { LoadingService } from 'src/app/services/spinner/loading.service';


@Component({
  selector: 'app-db-config',
  templateUrl: './db-config.component.html',
  styleUrls: ['./db-config.component.scss']
})
export class DbConfigComponent implements OnInit {
  public form: FormGroup;

  public visible = false;

  public databaseList = [];

  @Input()
  public dlgReference: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) private activeService: DialogData,
    private utilService: UtilService,
    private loadingService: LoadingService,
    private msgService: MessageService,
    private dbService: DbService,
    private indexService: IndexService,
  ) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  public connectDB(): void {
    this.loadingService.start('PROGRESSDLG');
    this.activeService.service.getDatabases(this.form.value).subscribe(
      response => {
        this.activeService.service.currentDB = this.form.value;
        this.updatecurrentMeta(this.indexService.currentIndex);
        this.visible = !this.visible;
        this.databaseList = response;
        this.loadingService.complete();
      },
      error => {
        this.utilService.showToast('Invalid Information', 'try again');
        this.loadingService.complete();
      }
    );
  }

  public closeDlg(): void {
    this.msgService.sendDlgMsg('CLOSE');
  }

  private buildForm(): void {
    this.form = new FormGroup({
      dbName: new FormControl('TPLGIS'),
      ip: new FormControl('172.16.44.74', Validators.required),
      port: new FormControl(5432, Validators.required),
      userName: new FormControl('postgres', Validators.required),
      password: new FormControl('Zima789!', Validators.required)
    });
  }

  private getFormValue(formName: string): any {
    return this.form.get(formName).value;
  }

  private updatecurrentMeta(meta: ElasticMeta): void {
    meta.databaseMeta.ip = this.getFormValue('ip');
    meta.databaseMeta.port = this.getFormValue('port');
    meta.databaseMeta.userName = this.getFormValue('userName');
  }
}
