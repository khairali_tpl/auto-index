import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService } from 'src/app/services/base/base.service';
import { Observable, ReplaySubject } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/models/role.model';
import { tap } from 'rxjs/operators';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {
  private _currentUser = {} as User;
  public userInfo: User;
  public userObs: ReplaySubject<User> = new ReplaySubject<User>();

  constructor(protected http: HttpClient) {
    super(http);
    this.endpoint = 'admin';
  }

  public createUser(user: User): Observable<any> {
    return this.post('user/createv2', user);
  }

  public getUsers(): Observable<User[]> {
    return this.get('user');
  }


  public getUserById(id: number): Observable<any> {
    return this.get('get_user?user_id=' + id);
  }

  updateUser(user: User): Observable<any> {
    return this.put('user/update', user);
  }

  updateUserSimple(user: User): Observable<any> {
    return this.put('user', user);
  }

  public getAccessTypes(): Observable<any> {
    return this.get('get_access_type_access_to');
  }

  public getAllRoles(): Observable<Role[]> {
    return this.get('role');
  }

  public disableUser(user_id): Observable<any> {
    return this.put('user?user_id=' + user_id);
  }

  public authenticate(email: string, password: string): Observable<User> {
    return this.get('login', { id: email, password }).pipe(
      tap(data => (this.currentUser = data))
    );
  }

  public updatePassword(currentPass, newPass): Observable<any> {
    return this.put('user/updatepassword?password=' + newPass + '&oldpassword=' + currentPass);
  }

  public enableUser(user_id): Observable<any> {
    return this.put('user/enable?user_id=' + user_id);
  }

  public get currentUser(): User {
    return this._currentUser;
  }

  public set currentUser(user: User) {
    this._currentUser = user;
  }
}
