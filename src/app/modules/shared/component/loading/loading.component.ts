import { Component, OnInit } from '@angular/core';
import { LoadingType, LoadingService } from 'src/app/services/spinner/loading.service';


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  public loading: LoadingType = 'COMPLETE';

  constructor(private loadingService: LoadingService) {
    this.loadingService.loadingEmitter$.subscribe(response => {
      this.loading = response;
    });
  }

  ngOnInit() {}
}
