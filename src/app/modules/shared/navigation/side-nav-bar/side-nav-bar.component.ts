import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { NavItem } from 'src/app/models/nav-item.model';
import { Router } from '@angular/router';
import { NavigationService } from 'src/app/modules/shared/navigation/navigation.service';
import { menuRotate } from 'src/app/animation/animation';

@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.scss'],
  animations: [menuRotate]
})
export class SideNavBarComponent {
  expanded: boolean;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: NavItem;
  @Input() depth: number;

  constructor(public navService: NavigationService, public router: Router) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  onItemSelected(item: NavItem) {
    if (!item.children || !item.children.length) {
      this.router.navigate([item.route]);
      // this.navService.closeNav();
    }
    if (item.children && item.children.length) {
      this.expanded = !this.expanded;
    }
  }
}
