import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './credential/login/login.component';
import { SharedModule } from './modules/shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { InterceptorService } from './services/interceptor/interceptor.service';
import { MatIconModule, MatButtonModule, MatTooltipModule } from '@angular/material';
import { SearchDemoComponent } from './search-demo/search-demo.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './services/user/user.service';
import { SpinnerComponent } from './spinner/spinner.component';
import { SimpleNotificationsModule } from 'angular2-notifications';

@NgModule({
  declarations: [AppComponent, LoginComponent, SearchDemoComponent, SpinnerComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    SharedModule,
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    SimpleNotificationsModule.forRoot(),
    NgxJsonViewerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    CookieService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
