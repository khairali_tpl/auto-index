import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../../../../services/util/util.service';
import { MessageService } from '../../../../services/message/message.service';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private msgService: MessageService
  ) {}

  public closeDlg(): void {
    this.msgService.sendDlgMsg('CLOSE');
  }
}
