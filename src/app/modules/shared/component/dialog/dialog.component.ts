import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  DialogData,
  UtilService
} from '../../../../services/util/util.service';
import { MessageService } from '../../../../services/message/message.service';
import { filter } from 'rxjs/operators';
import { LoadingService } from 'src/app/services/spinner/loading.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {
  public visible = false;
  public loading = 'COMPLETE';
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dlgData: DialogData,
    private msgService: MessageService,
    private loadingService: LoadingService
  ) {
    this.waitForMessage();
    this.progresOnHttp();
  }

  private progresOnHttp(): void {
    this.loadingService.loadingEmitter$.subscribe(response => {
      this.loading = response;
    });
  }
  private waitForMessage(): void {
    this.msgService.eventEmitter$
      .pipe(filter(data => data.status === 'CLOSE'))
      .subscribe(response => {
        this.dialogRef.close(response);
      });
  }
}
