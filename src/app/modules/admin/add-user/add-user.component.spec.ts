import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddUserComponent } from './add-user.component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from 'src/app/services/user/user.service';

describe('AddUserComponent', () => {
  let component: AddUserComponent;
  let fixture: ComponentFixture<AddUserComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCardModule,
        MatGridListModule,
        MatListModule,
        MatDividerModule,
        MatTabsModule,
        MatTableModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [AddUserComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserComponent);
    userService = TestBed.get(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('AddUser component should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should have fields username,password,email,company,phone,designation,purpose_of_usage ', () => {
    const flag =
      component.addForm.get('username').enabled &&
      component.addForm.get('password').enabled &&
      component.addForm.get('email').enabled &&
      component.addForm.get('company').enabled &&
      component.addForm.get('phone').enabled &&
      component.addForm.get('designation').enabled &&
      component.addForm.get('purpose_of_usage').enabled;
    expect(flag).toBe(true);
  });

  it('Calling create User from Service', () => {
    spyOn(userService, 'createUser').and.callThrough();
    component.onSubmit();
    expect(userService.createUser).toHaveBeenCalled();
  });
});
