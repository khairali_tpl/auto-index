import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './credential/login/login.component';
import { SearchDemoComponent } from './search-demo/search-demo.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'demo', component: SearchDemoComponent },
      {
        path: 'app',
        loadChildren: './modules/standard/standard.module#StandardModule'
      },
      {
        path: 'admin',
        loadChildren: './modules/admin/admin.module#AdminModule'
      },
      {
        path: 'routing',
        loadChildren: './modules/routing/routing.module#RoutingModule'
      }
    ]
  }
];

export const AppRoutingModule = RouterModule.forRoot(routes, { useHash: true });
