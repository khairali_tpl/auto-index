import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TilingRoutingModule } from './tiling-routing.module';

@NgModule({
  imports: [
    CommonModule,
    TilingRoutingModule
  ],
  declarations: []
})
export class TilingModule { }
