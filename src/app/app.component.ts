import { Component, OnDestroy } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router, RoutesRecognized, ActivationStart } from '@angular/router';
import { User } from './models/user.model';
import { UserService } from './services/user/user.service';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { SpinnerService } from './services/spinner/spinner.service';
import { NotificationsService } from 'angular2-notifications';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnDestroy {
  title = 'app';
  public isRequesting = false;
  private sub: any;
  public notificationOptions: any = {
    position: ['top', 'right'],
    timeOut: 5000
  };

    constructor(
      private cookieService: CookieService,
      private router: Router,
      private userService: UserService,
      public spinnerService: SpinnerService,
      private notifyService: NotificationsService
    ) {
        if (this.hasToken()) {
          this.iHaveToken();
        } else if (this.hasCredential()) {
          this.yesIremember();
        }
        this.sub = this.spinnerService.requestInProcess$.subscribe(
          isDone => {
            this.isRequesting = isDone;
          });
    }

    showProgress(val: boolean) {
      this.isRequesting = val;
    }

    private hasToken(): boolean {
      const accessToken = this.cookieService.get('access_token');
      return accessToken !== undefined && accessToken.length > 0;
    }

    private hasCredential(): boolean {
      const [email, password] = [
        this.cookieService.get('email'),
        this.cookieService.get('password')
      ];
      return (
        email !== undefined &&
        email.length > 0 &&
        password !== undefined &&
        password.length > 0
      );
    }

    private getModule(user: User): string {
      return user.accessRightes[0].module;
    }

    private iHaveToken(): void {
      this.router.events
        .filter(data => data instanceof RoutesRecognized)
        .take(1)
        .subscribe(route => {
          if (route['urlAfterRedirects'].includes('portal/home')) {
            this.router.navigate(['app/home']);
          } else {
            this.router.navigate([route['urlAfterRedirects']]);
          }
        });
    }

    private yesIremember(): void {
      this.userService
        .authenticate(
          this.cookieService.get('email'),
          this.cookieService.get('password')
        )
        .subscribe(
          response => {
            this.cookieService.set('access_token', response.token);
            const user = response as User;
            this.userService.userInfo = user;
            this.userService.userObs.next(response);
            this.router.navigate([this.getModule(user)]);
          },
          error => {
            // this.utilService.digestError(error);
          }
        );
    }
    ngOnDestroy() {
      this.sub.unsubscribe();
    }
}
