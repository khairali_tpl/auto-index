import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnInit,
  Inject
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UtilService, DialogData } from '../../../services/util/util.service';
import { MessageService } from '../../../services/message/message.service';
import { DbService } from '../../../services/db/db.service';
import { interval, pipe } from 'rxjs';
import { shakeAnim, shakeMe } from 'src/app/animation/animation';
import { take } from 'rxjs/operators';
import { IndexService } from 'src/app/services/index/index.service';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { LoadingService } from 'src/app/services/spinner/loading.service';

@Component({
  selector: 'app-elastic-config',
  templateUrl: './elastic-config.component.html',
  styleUrls: ['./elastic-config.component.scss'],
  animations: [shakeAnim]
})
export class ElasticConfigComponent implements OnInit {
  public form: FormGroup;

  public shakeState = 'inactive';

  public visible = false;

  public databaseList = [];

  @Input()
  public dlgReference: any;

  constructor(
    private loadingService: LoadingService,
    private utilService: UtilService,
    private msgService: MessageService,
    private indexService: IndexService
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  public connectElastic(): void {
    this.loadingService.start('PROGRESS');
    this.indexService.getIndicies(this.form.value).subscribe(
      resp => {
        this.loadingService.complete();
        this.msgService.sendDlgMsg('CLOSE', resp);
      },
      error => {
        this.shakeState = shakeMe(this.shakeState);
        this.loadingService.complete();
        this.utilService.showToast('Invalid Credentials', 'try again', {
          horizontalPosition: 'center',
          verticalPosition: 'bottom'
        });
      }
    );
  }

  public closeDlg(): void {
    this.msgService.sendDlgMsg('CLOSE');
  }

  private buildForm(): void {
    this.form = new FormGroup({
      host: new FormControl('localhost', Validators.required),
      port: new FormControl(9300, Validators.required),
      username: new FormControl('postgres', Validators.required),
      password: new FormControl('postgres', Validators.required),
      clusterName: new FormControl('postgres', Validators.required)
    });
  }
}
