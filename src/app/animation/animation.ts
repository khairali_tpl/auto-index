import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes,
  useAnimation
} from '@angular/animations';
import { shake, jello, fadeIn } from 'ng-animate';

export const anim = trigger('anim', [
  state(
    'state1',
    style({
      transform: 'scale(1)'
    })
  ),
  state(
    'state2',
    style({
      transform: 'scale(1)'
    })
  ),
  transition(
    ':enter',
    animate(
      '500ms ease-in',
      keyframes([
        style({ opacity: 0, transform: 'translateX(75%)', offset: 0 }),
        style({ opacity: 1, transform: 'translateX(35px)', offset: 0.5 }),
        style({ opacity: 1, transform: 'translateX(0)', offset: 1.0 })
      ])
    )
  )
]);

export const shakeAnim = trigger('shake', [
  transition('inactive <=> active', useAnimation(shake))
]);

export function shakeMe(shakeState: string): string {
  return (shakeState = shakeState === 'inactive' ? 'active' : 'inactive');
}

export const animMe = trigger('shake', [
  transition('void <=> *', useAnimation(fadeIn))
]);

export const menuRotate = trigger('indicatorRotate', [
  state('collapsed', style({ transform: 'rotate(0deg)' })),
  state('expanded', style({ transform: 'rotate(90deg)' })),
  transition(
    'expanded <=> collapsed',
    animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
  )
]);
