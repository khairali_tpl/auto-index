import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatInputModule,
  MatProgressBarModule,
  MatBadgeModule,
  MatTabsModule,
  MatSelectModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatPaginatorModule,
  MatChipsModule,
  MatGridListModule
} from '@angular/material';

import { RoutingRoutingModule } from './routing-routing.module';
import { RoutingbaseComponent } from './routingbase/routingbase.component';
import { CreateRoutingComponent } from './create-routing/create-routing.component';
import { SharedModule } from '../shared/shared.module';
import { DialogComponent } from '../shared/component/dialog/dialog.component';
// import { BaseComponent } from '../standard/base/base.component';

const MAT_MODULE = [
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatListModule,
  MatProgressBarModule,
  MatBadgeModule,
  MatTabsModule,
  MatSelectModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatPaginatorModule,
  MatChipsModule,
  MatGridListModule
];

@NgModule({
  imports: [CommonModule, RoutingRoutingModule, SharedModule, MAT_MODULE],
  declarations: [RoutingbaseComponent, CreateRoutingComponent],
  // entryComponents: [DialogComponent]
})
export class RoutingModule {
  constructor() {}
}
