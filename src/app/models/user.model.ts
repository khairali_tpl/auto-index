import { Widget } from './widget.model';

export interface User {
  user_id: number;
  username: string;
  email: string;
  password: string;
  company: string;
  phone: string;
  address: string;
  designation: string;
  purpose_of_usage: string;
  is_enable: Boolean;
  accessRightes: Widget[];
  token: string;
  roles: Role[];
}
export class UserImpl implements User {
  public user_id = undefined;
  public username = '';
  public email = '';
  public password = '';
  public company = '';
  public phone = '';
  public address = '';
  public designation = '';
  public purpose_of_usage = '';
  public is_enable = false;
  public time_stamp = undefined;
  public roles = [];
  public accessRightes = [];
  public token = undefined;
}

export interface Role {
  role_id: number;
  role_name: string;
  role_description: string;
  is_enable: boolean;
  time_stamp: any;
  widgets: Widget[];
}
