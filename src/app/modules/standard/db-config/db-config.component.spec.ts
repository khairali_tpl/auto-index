// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { DbConfigComponent } from './db-config.component';
// import { DbService } from '../../../services/db/db.service';
// import { UtilService } from '../../../services/util/util.service';
// import { MessageService } from '../../../services/message/message.service';
// import { DatabaseListComponent } from '../database-list/database-list.component';
// import { HttpClientTestingModule } from '@angular/common/http/testing';
// import {
//   MatInputModule,
//   MatListModule,
//   MatDividerModule,
//   MatTableModule,
//   MatDialogModule,
//   MatSnackBarModule
// } from '@angular/material';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { SchemaListComponent } from '../schema-list/schema-list.component';
// import { TableListComponent } from '../table-list/table-list.component';
// import { ColumnListComponent } from '../column-list/column-list.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// describe('DbConfigComponent', () => {
//   let component: DbConfigComponent;
//   let fixture: ComponentFixture<DbConfigComponent>;
//   let dbService: DbService;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         FormsModule,
//         ReactiveFormsModule,
//         MatInputModule,
//         MatListModule,
//         MatDividerModule,
//         MatTableModule,
//         MatDialogModule,
//         MatSnackBarModule,
//         BrowserAnimationsModule,
//         HttpClientTestingModule
//       ],
//       declarations: [
//         DbConfigComponent,
//         DatabaseListComponent,
//         SchemaListComponent,
//         TableListComponent,
//         ColumnListComponent
//       ],
//       providers: [UtilService, DbService, MessageService]
//     }).compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(DbConfigComponent);
//     component = fixture.componentInstance;
//     dbService = TestBed.get(DbService);
//     fixture.detectChanges();
//   });

//   it('DbConfigComponent should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('Form invalid when empty', () => {
//     expect(component.form.valid).toBeTruthy();
//   });

//   it('Database Name form field should exist', done => {
//     const dbName = component.form.controls['dbName'];
//     expect(dbName).toBeDefined();
//     done();
//   });

//   it('Port form field should exist', () => {
//     const port = component.form.controls['port'];
//     expect(port).toBeDefined();
//   });

//   it('Host/IP form field should exist', () => {
//     const ip = component.form.controls['ip'];
//     expect(ip).toBeDefined();
//   });

//   it('User Name form field should exist', () => {
//     const userName = component.form.controls['userName'];
//     expect(userName).toBeDefined();
//   });

//   it('Password form field should exist', () => {
//     const password = component.form.controls['password'];
//     expect(password).toBeDefined();
//   });

//   it('Form should be validate before service call', () => {
//     component.form.setValue({
//       dbName: 'any',
//       ip: 'localhost',
//       port: 5432,
//       userName: 'postgres',
//       password: 'postgres'
//     });
//     expect(component.form.valid).toBeTruthy();
//   });

//   it('Connect to DB', () => {
//     spyOn(dbService, 'getDatabases').and.callThrough();
//     component.connectDB();
//     expect(dbService.getDatabases).toHaveBeenCalled();
//   });
// });
