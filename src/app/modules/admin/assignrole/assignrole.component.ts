import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/models/role.model';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-assignrole',
  templateUrl: './assignrole.component.html',
  styleUrls: ['./assignrole.component.scss']
})
export class AssignroleComponent implements OnInit {
  users: User[];
  roles: Role[];
  assignForm: FormGroup;
  policy_list: any[] = [];
  products: any[] = [];

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.assignForm = this.formBuilder.group({
      user_id: ['', Validators.required],
      role_id: ['', Validators.required]
    });

    this.userService.getUsers().subscribe(
      data => {
        this.users = data;
      },
      error => {
      }
    );

    this.userService.getAllRoles().subscribe(
      roles_data => {
        this.roles = roles_data;
      },
      error => {
      }
    );
  }

  onSubmit() {
  }
}
