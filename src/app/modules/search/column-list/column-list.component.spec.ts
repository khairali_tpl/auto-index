// import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { DbService } from '../../../services/db/db.service';
// import { UtilService } from '../../../services/util/util.service';
// import { MessageService } from '../../../services/message/message.service';
// import { DatabaseListComponent } from '../database-list/database-list.component';
// import { HttpClientTestingModule } from '@angular/common/http/testing';
// import {
//   MatInputModule,
//   MatListModule,
//   MatDividerModule,
//   MatTableModule,
//   MatDialogModule,
//   MatSnackBarModule
// } from '@angular/material';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { TableListComponent } from '../table-list/table-list.component';
// import { ColumnListComponent } from '../column-list/column-list.component';
// import { NoopAnimationsModule } from '@angular/platform-browser/animations';
// import { DbInfo } from 'src/app/models/db-info.model';
// import { IndexInfo } from 'src/app/models/index-info.model';
// import { SchemaListComponent } from '../schema-list/schema-list.component';
// import { RouterTestingModule } from '@angular/router/testing';
// import { IndexService } from 'src/app/services/index/index.service';
// import { ElasticMeta } from 'src/app/models/elastic-meta.model';

// describe('ColumnListComponent', () => {
//   let component: ColumnListComponent;
//   let fixture: ComponentFixture<ColumnListComponent>;
//   let dbService: DbService;
//   let indexService: IndexService;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         FormsModule,
//         ReactiveFormsModule,
//         MatInputModule,
//         MatListModule,
//         MatDividerModule,
//         MatTableModule,
//         MatDialogModule,
//         MatSnackBarModule,
//         NoopAnimationsModule,
//         HttpClientTestingModule,
//         RouterTestingModule
//       ],
//       declarations: [
//         DatabaseListComponent,
//         ColumnListComponent,
//         TableListComponent,
//         ColumnListComponent,
//         SchemaListComponent
//       ],
//       providers: [UtilService, DbService, MessageService, IndexService]
//     }).compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ColumnListComponent);
//     component = fixture.componentInstance;
//     dbService = TestBed.get(DbService);
//     indexService = TestBed.get(IndexService);

//     dbService.currentDB = {
//       dbName: 'Database A',
//       schemaName: 'Schema A',
//       tableName: 'Table A',
//       columnsName: '',
//     } as DbInfo;

//     indexService.currentIndex = {} as ElasticMeta;

//     fixture.detectChanges();
//   });

//   it('Columns List Component Should Create', () => {
//     expect(component).toBeTruthy();
//   });

//   it('Columns List should be filled', () => {
//     component.columns = ['Column A', 'Column B', 'Column C', 'Column D'];
//     expect(component.columns.length).toBeGreaterThan(0);
//   });

//   it('Before next step ,one or multiple column should be selected ', () => {
//     indexService.currentIndex.databaseMeta.columnsName = 'eee';
//     expect(indexService.currentIndex.databaseMeta.columnsName.length).toBeGreaterThan(0);
//   });
// });
