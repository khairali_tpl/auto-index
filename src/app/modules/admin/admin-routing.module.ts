import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from './add-user/add-user.component';
import { AdminbaseComponent } from './adminbase/adminbase.component';
import { ListUserComponent } from './list-user/list-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AssignpolicyComponent } from './assignpolicy/assignpolicy.component';
import { AssignroleComponent } from './assignrole/assignrole.component';
import { BaseComponent } from '../standard/base/base.component';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
      { path: '', redirectTo: 'list-user' },
      { path: 'list-user', component: ListUserComponent },
      { path: 'add-user', component: AddUserComponent },
      { path: 'edit-user/:id', component: EditUserComponent },
      { path: 'assign-policy', component: AssignpolicyComponent },
      { path: 'assign-role', component: AssignroleComponent }
    ]
  }
];

export const AdminRoutingModule = RouterModule.forChild(routes);
