import { NavItem } from './nav-item.model';


export class Navigation {
  public static navItems: NavItem[] = [
    {
      displayName: 'Dashboard',
      iconName: 'dashboard',
      route: 'app/home',
      role: 'other'
    },
    {
      displayName: 'Indices',
      iconName: 'storage',
      route: 'app/indices',
      role: 'other'
    },
    // ADMIN
    {
      displayName: 'List User',
      iconName: 'group',
      route: 'admin/list-user',
      role: 'admin'
    },
    {
      displayName: 'Create User',
      iconName: 'person_add',
      route: 'admin/add-user',
      role: 'admin'
    },
    {
      displayName: 'Assign Policy',
      iconName: 'accessible',
      route: 'admin/assign-policy',
      role: 'admin'
    },
    {
      displayName: 'Assign Role',
      iconName: 'work',
      route: 'admin/assign-role',
      role: 'admin'
    },
    {
      displayName: 'Routing Config',
      iconName: 'settings',
      route: 'routing/create-routing',
      role: 'other'
    }
    // },
    // {
    //     displayName: 'Disney',
    //     iconName: 'videocam',
    //     children: [
    //         {
    //             displayName: 'Speakers',
    //             iconName: 'group',
    //             children: [
    //                 {
    //                     displayName: 'Michael Prentice',
    //                     iconName: 'person',
    //                     // route: 'michael-prentice',
    //                     children: [
    //                         {
    //                             displayName: 'Create Enterprise UIs',
    //                             iconName: 'star_rate',
    //                             // route: 'material-design'
    //                         }
    //                     ]
    //                 },
    //                 {
    //                     displayName: 'Stephen Fluin',
    //                     iconName: 'person',
    //                     // route: 'stephen-fluin',
    //                     children: [
    //                         {
    //                             displayName: 'What\'s up with the Web?',
    //                             iconName: 'star_rate',
    //                             // route: 'what-up-web'
    //                         }
    //                     ]
    //                 },
    //                 {
    //                     displayName: 'Mike Brocchi',
    //                     iconName: 'person',
    //                     // route: 'mike-brocchi',
    //                     children: [
    //                         {
    //                             displayName: 'My ally, the CLI',
    //                             iconName: 'star_rate',
    //                             // route: 'my-ally-cli'
    //                         },
    //                         {
    //                             displayName: 'Become an Angular Tailor',
    //                             iconName: 'star_rate',
    //                             // route: 'become-angular-tailer'
    //                         }
    //                     ]
    //                 }
    //             ]
    //         },
    //         {
    //             displayName: 'Sessions',
    //             iconName: 'speaker_notes',
    //             children: [
    //                 {
    //                     displayName: 'Create Enterprise UIs',
    //                     iconName: 'star_rate',
    //                     // route: 'material-design'
    //                 },
    //                 {
    //                     displayName: 'What\'s up with the Web?',
    //                     iconName: 'star_rate',
    //                     // route: 'what-up-web'
    //                 },
    //                 {
    //                     displayName: 'My ally, the CLI',
    //                     iconName: 'star_rate',
    //                     // route: 'my-ally-cli'
    //                 },
    //                 {
    //                     displayName: 'Become an Angular Tailor',
    //                     iconName: 'star_rate',
    //                     // route: 'become-angular-tailer'
    //                 }
    //             ]
    //         },
    //         {
    //             displayName: 'Feedback',
    //             iconName: 'feedback',
    //             // route: 'feedback'
    //         }
    //     ]
    // }
  ];
}

