import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxJsonViewerModule, NgxJsonViewerComponent } from 'ngx-json-viewer';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatInputModule,
  MatProgressBarModule,
  MatBadgeModule,
  MatTabsModule,
  MatSelectModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatPaginatorModule,
  MatChipsModule,
  MatTableModule,
  MatSlideToggleModule,
  MatAutocompleteModule,
  MatCheckboxModule
} from '@angular/material';

import { SharedRoutingModule } from './shared-routing.module';
import { ConfirmDialogComponent } from './component/confirm-dialog/confirm-dialog.component';
import { MessageDialogComponent } from './component/message-dialog/message-dialog.component';
import { DbConfigComponent } from '../standard/db-config/db-config.component';
import { RoutingConfigComponent } from '../standard/routing-config/routing-config.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { TableListComponent } from '../standard/table-list/table-list.component';
import { DialogComponent } from './component/dialog/dialog.component';
import { ColumnListComponent } from '../standard/column-list/column-list.component';
import { HttpClientModule } from '@angular/common/http';
import { DatabaseListComponent } from '../standard/database-list/database-list.component';
import { SchemaListComponent } from '../standard/schema-list/schema-list.component';
import { ElasticConfigComponent } from '../standard/elastic-config/elastic-config.component';
import { TopToolBarComponent } from './navigation/top-tool-bar/top-tool-bar.component';
import { SideNavBarComponent } from './navigation/side-nav-bar/side-nav-bar.component';
import { SynonymsDialogComponent } from '../standard/synonyms/synonyms-dialog/synonyms-dialog.component';
import { UpdatePasswordComponent } from '../standard/view-profile/update-password/update-password.component';
import { LoadingComponent } from './component/loading/loading.component';
import { BaseComponent } from '../standard/base/base.component';

const MAT_MODULE = [
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatProgressBarModule,
  MatBadgeModule,
  MatTabsModule,
  MatSelectModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatMenuModule,
  MatPaginatorModule,
  MatChipsModule,
  MatTableModule,
  MatSlideToggleModule,
  MatAutocompleteModule,
  MatCheckboxModule
];

const APP_MODULE = [FormsModule, ReactiveFormsModule];
const COMPONENTS = [
  ConfirmDialogComponent,
  MessageDialogComponent,
  DialogComponent,
  DbConfigComponent,
  UpdatePasswordComponent,
  RoutingConfigComponent,
  ElasticConfigComponent,
  OnlyNumberDirective,
  TableListComponent,
  DatabaseListComponent,
  SchemaListComponent,
  ColumnListComponent,
  LoadingComponent,
  TopToolBarComponent,
  SideNavBarComponent,
  BaseComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule,
    HttpClientModule,
    MAT_MODULE,
    APP_MODULE,
    NgxJsonViewerModule
  ],
  declarations: [COMPONENTS],
  exports: [MAT_MODULE, APP_MODULE, COMPONENTS],
  entryComponents: [
    MessageDialogComponent,
    DialogComponent,
    DbConfigComponent,
    UpdatePasswordComponent,
    RoutingConfigComponent,
    ElasticConfigComponent
  ]
})
export class SharedModule {}
