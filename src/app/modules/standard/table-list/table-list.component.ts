import { Component, OnInit, ViewChild, Inject } from '@angular/core';

import {
  MatSelectionList,
  MatListOption,
  MAT_DIALOG_DATA
} from '@angular/material';
import { anim } from '../../../animation/animation';
import { DbService } from '../../../services/db/db.service';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/services/message/message.service';
import { IndexService } from 'src/app/services/index/index.service';
import { RoutingService } from 'src/app/services/routing/routing.service';
import { DialogData } from 'src/app/services/util/util.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss'],
  animations: [anim]
})
export class TableListComponent implements OnInit {
  @ViewChild(MatSelectionList)
  tableRef: MatSelectionList;

  public visible = false;

  public state = 'state1';

  public tables: string[] = [];

  public selectedOption: MatListOption;

  constructor(
    @Inject(MAT_DIALOG_DATA) private activeService: DialogData,
    private dbService: DbService,
    private routingService: RoutingService,
    private indexService: IndexService,
    private route: Router,
    private msgService: MessageService
  ) {
    this.animateMe();
  }

  public ngOnInit(): void {
    this.activeService.service
      .getTables(this.dbService.currentDB.schemaName)
      .subscribe(response => {
        this.tables = response;
      });
  }

  public next(): void {
    this.dbService.currentDB.tableName = this.getSelectedValue();
    this.setTableName();
    if (this.route.url.includes('routing')) {
      this.msgService.sendDlgMsg('CLOSE');
    } else {
      this.visible = !this.visible;
    }
  }

  public changeSelect($evt): void {
    this.tableRef.deselectAll();
    this.tableRef.selectedOptions.toggle($evt.option);
    this.selectedOption = $evt.option;
  }

  public animateMe(): void {
    this.state = this.state === 'state1' ? 'state2' : 'state1';
  }

  public setTableName(): void {
    this.indexService.currentIndex.databaseMeta.tableName = this.getSelectedValue();
  }

  private getSelectedValue(): string {
    return this.selectedOption.getLabel().trim();
  }
}
