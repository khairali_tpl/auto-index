export function getElasticConfig(): any {
  return {
    // host: '10.1.2.195',
    host: '172.16.44.71',
    port: 9300,
    username: null,
    password: null,
    clusterName: 'docker-cluster',
    shards: '5',
    replicas: '5',
    databaseMeta: {},
    databaseInfo: {
      limit: 13,
      offset: 0
    }
  };
}
