'use strict';

import { Component, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnDestroy {

  public currentTimeout: number;
  public isDelayedRunning = false;

  @Input()
  public delay = 300;

  @Input()
  public set isRunning(value: boolean) {
      if (!value) {
          this.cancelTimeout();
          this.isDelayedRunning = false;
          return;
      }

      if (this.currentTimeout) {
          return;
      }

      this.currentTimeout = Number.parseInt(setTimeout(() => {
          this.isDelayedRunning = value;
          this.cancelTimeout();
      }, this.delay).toString());
  }

  public cancelTimeout(): void {
      clearTimeout(this.currentTimeout);
      this.currentTimeout = undefined;
  }

  ngOnDestroy(): any {
      this.cancelTimeout();
  }

}
