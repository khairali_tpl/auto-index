import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankingComponent } from './ranking.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatDialogModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbar,
  MatToolbarModule,
  MatOptionModule,
  MatSelectModule,
  MatCardModule,
  MatExpansionModule,
  MatChipsModule,
  MatIconModule,
  MatSlideToggleModule,
  MatAutocompleteModule,
  MatCheckboxModule
} from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { UtilService } from 'src/app/services/util/util.service';
import { DbService } from 'src/app/services/db/db.service';
import { MessageService } from 'src/app/services/message/message.service';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

describe('RankingComponent', () => {
  let component: RankingComponent;
  let fixture: ComponentFixture<RankingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDividerModule,
        MatTableModule,
        MatDialogModule,
        MatSnackBarModule,
        MatToolbarModule,
        MatOptionModule,
        MatSelectModule,
        MatCardModule,
        MatChipsModule,
        MatIconModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      declarations: [RankingComponent],
      providers: [
        { provide: HttpClient, useValue: {} },
        UtilService,
        DbService,
        MessageService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankingComponent);
    component = fixture.componentInstance;
    component.indexService.currentIndex.databaseMeta.columns = ['a', 'b', 'c'];
    fixture.detectChanges();
  });

  it('Ranking component should create', () => {
    expect(component).toBeTruthy();
  });

  // it('Should have Searchable Attributes , Ranking Formula , Type Tolerance & Optional Words form ', () => {
  //   const compiled = fixture.debugElement.queryAll(By.css('mat-toolbar'));
  //   expect(compiled.length).toBe(4);
  // });

  it('Searchable Columns field should exist', () => {
    const port = component.form.controls['searchableColumns'];
    expect(port).toBeDefined();
  });

  it('Rank Columns field should exist', () => {
    const port = component.form.controls['rankColumns'];
    expect(port).toBeDefined();
  });

  it('Type Tolerance  field should exist', () => {
    const port = component.form.controls['typoTolerance'];
    expect(port).toBeDefined();
  });

  it('Prefix length  field should exist', () => {
    const port = component.form.controls['pre_length'];
    expect(port).toBeDefined();
  });

  it('Ignore Plural  field should exist', () => {
    const port = component.form.controls['ignore_plurals'];
    expect(port).toBeDefined();
  });

  it('Remove stopwords  field should exist', () => {
    const port = component.form.controls['remove_stopwords'];
    expect(port).toBeDefined();
  });

  it('Optional Words  field should exist', () => {
    const port = component.form.controls['optionalWords'];
    expect(port).toBeDefined();
  });

  it('Transpositon checkbox should exist', () => {
    const checkbox = component.form.controls['trans_pos'];
    expect(checkbox).toBeDefined();
  });

  it('should have autocomplete field and perform correctly by click on option', () => {
    const toggle = fixture.nativeElement.querySelector('.toggle-margin');
    toggle.click();
    component.isRemoveStopwords = true;
    fixture.detectChanges();
    const input = fixture.nativeElement.querySelector('#sw-input');
    input.focus();
    input.dispatchEvent(new Event('focusin'));
    fixture.detectChanges();
    const option = fixture.nativeElement.querySelectorAll('.mat-option');
    expect(option).toBeTruthy();
  });

  it('should expand the panel of ranking default criteria', () => {
    const panel = fixture.nativeElement.querySelector('#ranking-exp-panel');
    let isExpanded = fixture.nativeElement.querySelector('.mat-expanded');
    expect(isExpanded).toBeFalsy();
    panel.click();
    fixture.whenStable();
    fixture.detectChanges();
    isExpanded = fixture.nativeElement.querySelector('.mat-expanded');
    expect(isExpanded).toBeTruthy();
  });

  it('should have multi select option and check drop down length', async () => {
    const trigger = fixture.debugElement.query(By.css('#disable-typo-select')).nativeElement;
    trigger.click();
    fixture.detectChanges();
    await fixture.whenStable().then(() => {
        const inquiryOptions = fixture.debugElement.queryAll(By.css('.mat-option-text'));
      expect(inquiryOptions.length).toEqual(3);
      const option1 = fixture.debugElement.query(By.css('#opt-1')).nativeElement;
      option1.click();
      const option2 = fixture.debugElement.query(By.css('#opt-2')).nativeElement;
      option2.click();
      const head = fixture.debugElement.query(By.css('#typo-head')).nativeElement;
      head.click();
      fixture.detectChanges();
      const selectedText = fixture.nativeElement.querySelector('#typo-txt');
      expect(selectedText.innerHTML).toContain('other');
    });
});
});
