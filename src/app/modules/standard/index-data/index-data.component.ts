import { Component, OnInit, ViewChild } from '@angular/core';
import { IndexService } from 'src/app/services/index/index.service';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { DbInfo } from 'src/app/models/db-info.model';
import { BehaviorSubject } from 'rxjs';
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Component({
  selector: 'app-index-data',
  templateUrl: './index-data.component.html',
  styleUrls: ['./index-data.component.scss']
})
export class IndexDataComponent implements OnInit {
  public offset: any;
  public totalRecords: any;
  public paginationDetail: any;
  public temp: any;
  public object: ElasticMeta;

  constructor(
    private indexService: IndexService // private elasticMetaa: ElasticMeta
  ) {
  }

  @ViewChild('paginator') paginator;

  ngOnInit() {
    this.totalRecords = 0;
    this.temp = [];
    this.object = this.elasticMeta;
    this.getDocs(this.object);
  }

  // function called on next page
  public getNext(event) {
    this.offset = event.pageSize * event.pageIndex;
    this.object = this.elasticMeta;
    this.object.databaseInfo.offset = event.pageSize * event.pageIndex;
    this.getDocs(this.object);
  }

  public getDocs(elasticMeta) {
    this.indexService.getDocuments(elasticMeta).subscribe(
      response => {
        this.paginator.length = 10000;
        this.temp = response.hits.hits;
      },
      () => {
      }
    );
  }

  public getJSON(any): JSON {
    return any;
  }

  public get elasticMeta(): ElasticMeta {
    return {
      indexName: 'tplsearch',
      host: '172.16.44.71',
      port: '9300',
      username: null,
      password: null,
      clusterName: 'elasticsearch',
      shards: '',
      replicas: '',
      databaseInfo: this.dbInfo,
      mapping: ''
    } as ElasticMeta;
  }

  public get dbInfo(): DbInfo {
    return {
      columns: [],
      where: '',
      limit: 10,
      offset: 0
    } as DbInfo;
    // return this.dbInfo;
  }

  public set dbInfo(info: DbInfo) {
    this.dbInfo.columns = [];
    this.dbInfo.where = info.where;
    this.dbInfo.limit = info.limit;
    this.dbInfo.offset = info.offset;
  }
}
