import { Routes, RouterModule } from '@angular/router';
import { BaseComponent } from './base/base.component';
import { HomeComponent } from './home/home.component';
import { IndiciesComponent } from './indicies/indicies.component';
import { SelectedIndexComponent } from './selected-index/selected-index.component';
import { IndexDataComponent } from './index-data/index-data.component';
import { RankingComponent } from './ranking/ranking.component';
import { SynonymsComponent } from './synonyms/synonyms.component';
import { IndexResolve } from 'src/app/resolver/index/index-resolve.service';
import { ViewProfileComponent } from './view-profile/view-profile.component';
const data = { type: 'SEARCH' };
const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
      { path: '', redirectTo: 'home' },
      { path: 'home', component: HomeComponent, data },
      { path: 'indices', component: IndiciesComponent, data },
      { path: 'view-profile', component: ViewProfileComponent },
      {
        path: 'selected-index/:indexname',
        component: SelectedIndexComponent,
        resolve: {
          data: IndexResolve
        },
        children: [
          { path: '', redirectTo: 'data' },
          { path: 'data', component: IndexDataComponent },
          { path: 'ranking', component: RankingComponent },
          { path: 'synonyms', component: SynonymsComponent }
        ]
      }
    ]
  }
];

export const StandardRoutingModule = RouterModule.forChild(routes);
