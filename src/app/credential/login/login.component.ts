import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util/util.service';
import { TableListComponent } from '../../modules/standard/table-list/table-list.component';
import { animMe } from 'src/app/animation/animation';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/user.model';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [animMe]
})

export class LoginComponent implements OnInit {
  public form: FormGroup;
  cookieValue = 'UNKNOWN';

  constructor(
    public router: Router,
    public fb: FormBuilder,
    public util: UtilService,
    public userService: UserService,
    private cookieService: CookieService
  ) {}

  public ngOnInit() {
    this.buildForm();
  }

  public authenticate(): void {
    const { email, password } = this.form.value;
    this.userService.authenticate(email, password).subscribe(response => {
      this.cookieService.set( 'current_user', JSON.stringify(response) );
      this.isAdmin(response)
        ? this.router.navigate(['/admin'])
        : this.router.navigate(['/app']);
    });
  }

  private buildForm(): void {
    this.form = this.fb.group({
      email: new FormControl('iddexottyss-2972@yopmail.com', Validators.required),
      password: new FormControl('123456', Validators.required)
    });
  }

  private isAdmin(user: User): boolean {
    this.persistToken(user.token);
    return user.roles.some(data =>
      data.role_name.toLowerCase().includes('admin')
    );
  }

  private persistToken(token: string): void {
    sessionStorage.setItem('access_token', token);
  }
}
