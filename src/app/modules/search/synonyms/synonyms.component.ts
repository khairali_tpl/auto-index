import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IndexService } from 'src/app/services/index/index.service';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { SynonymsDialogComponent } from './synonyms-dialog/synonyms-dialog.component';
import { UtilService } from 'src/app/services/util/util.service';

@Component({
  selector: 'app-synonyms',
  templateUrl: './synonyms.component.html',
  styleUrls: ['./synonyms.component.scss']
})
export class SynonymsComponent implements OnInit {
  public form: FormGroup;
  displayedColumns: string[] = ['type', 'expansion', 'actions'];

  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    public indexService: IndexService,
    public dialog: MatDialog,
    public utilService: UtilService
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  public buildForm(): void {
    this.form = new FormGroup({
      searchableColumns: new FormControl(undefined),
      rankColumns: new FormControl(undefined),
      minChar: new FormControl(undefined),
      ignorePlural: new FormControl(undefined, Validators.required),
      typoTolerance: new FormControl(undefined, Validators.required),
      removeStopWords: new FormControl(undefined, Validators.required),
      optionalWords: new FormControl(undefined, Validators.required)
    });
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(SynonymsDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource.data = this.dataSource.data.concat(result);
        this.indexService.currentIndex.setting.settings.index.analysis.filter.synonym = this
          .createSynonyms(this.dataSource.data);
      }
    });
  }

  public createSynonyms(synonyms: string[]): any {
    return {
      type: 'synonym',
      synonyms
    };
  }
}
