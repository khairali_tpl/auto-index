import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { IndexDataComponent } from './index-data.component';
import {
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBarModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule,
  MatPaginatorModule,
  MatSlideToggleModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxJsonViewerModule } from 'ngx-json-viewer';

describe('IndexDataComponent', () => {
  let component: IndexDataComponent;
  let fixture: ComponentFixture<IndexDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [

        MatInputModule,
        MatCardModule,
        MatPaginatorModule,
        MatGridListModule,
        MatListModule,
        MatDividerModule,
        MatTabsModule,
        MatTableModule,
        MatSlideToggleModule,
        MatDialogModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        NgxJsonViewerModule,
        HttpClientTestingModule
      ],
      declarations: [ IndexDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Paginator', () => {
    expect(component.paginator.toBeTruthy);
  });
  it('Pagination Size', () => {
    expect(component.paginator.pageSize).toEqual(10);
  });

});
