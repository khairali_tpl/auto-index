import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { getElasticConfig } from 'src/app/config/app-config';

@Injectable({
  providedIn: 'root'
})
export class IndexService extends BaseService {
  private _currentIndex = getElasticConfig();

  constructor(protected http: HttpClient) {
    super(http);
  }

  public createIndex(
    onlyPersistInDB: boolean,
    elasticMeta: ElasticMeta
  ): Observable<any> {
    return this.post(`elastic/index/create/${onlyPersistInDB}`, elasticMeta);
  }

  /**
   *
   * @param elasticMeta ElasticMeta
   * @param isFromDB default value true
   */
  public getIndicies(
    elasticMeta: ElasticMeta,
    isFromDB = false
  ): Observable<ElasticMeta[]> {
    return this.post(`elastic/indices/${isFromDB}`, elasticMeta);
  }

  public getDocuments(elasticMeta: ElasticMeta): Observable<any> {
    return this.post(`elastic/index/search`, elasticMeta); // .pipe(
  }

  public getIndexDetail(indexName: string): Observable<ElasticMeta> {
    return this.get(`elastic/index/detail/${indexName}`);
  }

  public searchData(queryStrng: string): Observable<any> {
    const param = {
      size: 5,
      query: {
        query_string: {
          query: queryStrng,
          fields: ['firstname', 'address']
        }
      }
    };
    return this.http.post('http://localhost:9200/bank/_search?pretty', param);
  }

  /** temporary delete on confirm success of get list of index from elastic  */
  private createIndexList(data: any): any[] {
    const indicies = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        indicies.push({
          name: key,
          total: data[key]
        });
      }
    }
    return indicies;
  }

  public getCurrentIndexName(): string {
    return this.currentIndex.indexName;
  }
  get currentIndex(): ElasticMeta {
    return this._currentIndex;
  }

  set currentIndex(currentIndex: ElasticMeta) {
    this._currentIndex = currentIndex;
  }

  get currentDBMeta(): any {
    return this.currentIndex.databaseMeta;
  }

}
