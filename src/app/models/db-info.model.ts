export interface DbInfo {
  ip: string;
  port: number;
  userName: string;
  password: string;
  dbName: string;
  schemaName: string;
  tableName: string;
  columns: string[];
  mapping: string;
  where: string;
  limit: number;
  offset: number;
}
