import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynonymsComponent } from './synonyms.component';
import {
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatDialog,
  MatPaginatorModule,
  MatTableModule,
  MatDialogModule,
  MatSnackBar
} from '@angular/material';
import { IndexService } from 'src/app/services/index/index.service';
import { UtilService } from 'src/app/services/util/util.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { by, element } from 'protractor';
import { HttpClient } from '@angular/common/http';
import { MessageService } from 'src/app/services/message/message.service';

describe('SynonymsComponent', () => {
  let component: SynonymsComponent;
  let fixture: ComponentFixture<SynonymsComponent>;
  let addBtn: DebugElement;
  let table: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatToolbarModule,
        MatCardModule,
        MatPaginatorModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule,
        RouterTestingModule,
        HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [SynonymsComponent],
      providers: [
        MatDialog,
        IndexService,
        UtilService,
        MatSnackBar,
        { provide: HttpClient, useValue: {} },
        UtilService,
        MessageService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create synonyms component', () => {
    expect(component).toBeTruthy();
  });

  it('should have add synonym button', () => {
    addBtn = fixture.debugElement.query(By.css('button')).nativeElement;
    expect(addBtn).toBeTruthy();
  });

  it('should have synonyms table', () => {
    table = fixture.debugElement.query(By.css('table'));
    expect(table.nativeElement).toBeTruthy();
  });

  it('should have synonyms table correct header and paginator', () => {
    const el = fixture.nativeElement.querySelector('.mat-table thead tr th');
    expect(el.classList).toContain('mat-column-type');

    const tblPaginator = fixture.nativeElement.querySelector('.mat-paginator');
    expect(tblPaginator).toBeTruthy();
  });
});
