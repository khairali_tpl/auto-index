import { Component, OnInit } from '@angular/core';
import { UtilService, hasValue } from '../../../services/util/util.service';
import { DbConfigComponent } from '../db-config/db-config.component';
import { Router } from '@angular/router';
import { ElasticConfigComponent } from '../elastic-config/elastic-config.component';
import { IndexService } from 'src/app/services/index/index.service';
import { getElasticConfig } from 'src/app/config/app-config';
import { DbService } from 'src/app/services/db/db.service';
import { ElasticMeta } from 'src/app/models/elastic-meta.model';
import { SpinnerService } from 'src/app/services/spinner/spinner.service';

@Component({
  selector: 'app-indicies',
  templateUrl: './indicies.component.html',
  styleUrls: ['./indicies.component.scss']
})
export class IndiciesComponent implements OnInit {
  public indicies = [] as ElasticMeta[];

  public init = true;

  constructor(
    public dbService: DbService,
    public utilService: UtilService,
    public spinnerService: SpinnerService,
    public indexService: IndexService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.spinnerService.requestInProcess(true);
    this.indexService.getIndicies(getElasticConfig(), true).subscribe(
      response => {
        if (hasValue(response)) {
          this.indicies = response;
        }
        this.init = false;
        this.spinnerService.requestInProcess(false);
      },
      error => {
        this.spinnerService.requestInProcess(false);
      },
      () => this.spinnerService.requestInProcess(false)
    );
  }

  public selectIndex(idx: ElasticMeta): void {
    this.router.navigate([`app/selected-index/${idx.indexName}`]);
  }

  public addNewIndex(): void {
    const config = {
      width: '500px',
      disableClose: false,
      data: {
        component: DbConfigComponent,
        service: this.dbService,
        serviceName: 'DbService'
      }
    };

    this.utilService.createDialog('CUSTOM', config);
  }

  public configureElastic(): void {
    const config = {
      width: '500px',
      disableClose: false,
      data: {
        component: ElasticConfigComponent
      }
    };
    this.utilService.createDialog('CUSTOM', config).subscribe(response => {
      this.init = false;
      this.indicies = response['data'];
    });
  }
}
