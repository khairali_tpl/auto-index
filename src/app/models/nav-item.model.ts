export class NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  role?: string;
  children?: NavItem[];
}
