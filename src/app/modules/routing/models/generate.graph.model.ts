export class GenerateGraph {
  restrictedColumnName: string;
  priorityColumnName: string;
  usageColumnName: string;
  speedColumnName: string;
  directionColumnName: string;
  nameColumn: string;
  roundaboutIdentification: string;
  uturnIdentification: string;
  directionIdentification: string;
  restrictedIdentification: string;
  semiRestrictedIdentification: string;
}
