export interface Widget {
    widget_id: any;
    module: string;
    component_selector: string;
    url: string;
    description: string;
    is_enable: boolean;
    time_stamp: number;

}

export class WidgetImpl implements Widget {
    public widget_id = undefined;
    public module = '';
    public component_selector = '';
    public url = '';
    public description = '';
    public is_enable = true;
    public time_stamp = undefined;
}
