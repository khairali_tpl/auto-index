import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { DbInfo } from '../../models/db-info.model';

@Injectable({
  providedIn: 'root'
})
export class DbService extends BaseService {
  private _currentDB: DbInfo;

  constructor(protected http: HttpClient) {
    super(http);
    this.endpoint = 'dbmanager';
  }
  public get currentDB(): DbInfo {
    return this._currentDB;
  }

  public set currentDB(data: DbInfo) {
    this._currentDB = data;
  }

  public getDatabases(dbInfo: DbInfo): Observable<DbInfo[]> {
    return this.post('getconnection', dbInfo);
  }

  public getSchemas(dbInfo: DbInfo): Observable<string[]> {
    return this.get('getschemas', {
      dbName: dbInfo.dbName,
      userName: dbInfo.userName,
      password: dbInfo.password
    });
  }

  public getTables(schemaName: String): Observable<string[]> {
    return this.get('gettables', { schemaName });
  }

  public getcolumns(dbInfo: DbInfo): Observable<string[]> {
    const schemaName = dbInfo.schemaName;
    const tableName = dbInfo.tableName;
    return this.get('getcolumns', { schemaName, tableName });
  }

  public getIndecies(): Observable<any> {
    return of([
      'index A',
      'index B',
      'index C',
      'index D',
      'index E',
      'index F'
    ]);
  }
}
