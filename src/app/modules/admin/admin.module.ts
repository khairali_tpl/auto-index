import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { AddUserComponent } from './add-user/add-user.component';
import { SharedModule } from '../shared/shared.module';
import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { AdminbaseComponent } from './adminbase/adminbase.component';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatTreeModule,
  MatCheckboxModule,
  MatGridListModule,
  MatSlideToggleModule
} from '@angular/material';
import { ListUserComponent } from './list-user/list-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AssignpolicyComponent } from './assignpolicy/assignpolicy.component';
import { AssignroleComponent } from './assignrole/assignrole.component';
// import { BaseComponent } from '../standard/base/base.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    SharedModule,
    LayoutModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatTreeModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSlideToggleModule
  ],
  providers: [],
  declarations: [
    AddUserComponent,
    AdminbaseComponent,
    ListUserComponent,
    EditUserComponent,
    AssignpolicyComponent,
    AssignroleComponent,
    // BaseComponent
  ]
})
export class AdminModule {
  constructor() {}
}
