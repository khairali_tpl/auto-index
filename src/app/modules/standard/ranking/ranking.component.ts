import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  MatSelectChange,
  MatChipInputEvent,
  MatAutocomplete,
  MatAutocompleteSelectedEvent
} from '@angular/material';
import { IndexService } from 'src/app/services/index/index.service';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';

const CRITERIA = [
  'typo',
  'geo',
  'words',
  'filters',
  'proximity',
  'attribute',
  'exact',
  'custom'
];

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent {
  public form: FormGroup;
  // selected column
  public columns: string[] = [];

  public polarity: boolean[] = [true, false];

  public rankingCriterias = CRITERIA;

  public customRanking = [];

  public isRemoveStopwords = false;

  // public indexInfo = {} as ElasticMeta;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [];
  swCtrl = new FormControl();
  filteredStopWords: Observable<string[]>;
  stopWords: string[] = [];
  rankings = [] as string[];
  searchAbleAttr = [] as string[];
  fuzziness: any[] = [
    { value: 'AUTO', viewValue: 'AUTO' },
    { value: '0', viewValue: 'ZERO' },
    { value: '1', viewValue: 'ONE' },
    { value: '2', viewValue: 'TWO' }
  ];

  allStopWordsLang: string[] = [
    '_arabic_',
    '_armenian_',
    '_basque_',
    '_bengali_',
    '_brazilian_',
    '_bulgarian_',
    '_catalan_',
    '_czech_',
    '_danish_',
    '_dutch_',
    '_english_',
    '_finnish_',
    '_french_',
    '_galician_',
    '_german_',
    '_greek_',
    '_hindi_',
    '_hungarian_',
    '_indonesian_',
    '_irish_',
    '_italian_',
    '_latvian_',
    '_norwegian_',
    '_persian_',
    '_portuguese_',
    '_romanian_',
    '_russian_',
    '_sorani_',
    '_spanish_',
    '_swedish_',
    '_thai_',
    '_turkish_'
  ];

  @ViewChild('stopWordInput') stopWordInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    public indexService: IndexService,
    public activeRoute: ActivatedRoute
  ) {
    this.buildForm();
    this.filteredStopWords = this.swCtrl.valueChanges.pipe(
      startWith(null),
      map((stopWord: string | null) =>
        stopWord ? this._filter(stopWord) : this.allStopWordsLang.slice()
      )
    );
    this.typoDisable();
  }

  add(event: MatChipInputEvent): void {
    // Add stop word only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our sw
      if ((value || '').trim()) {
        this.stopWords.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.swCtrl.setValue(null);
    }
  }

  remove(stopWord: string): void {
    const index = this.stopWords.indexOf(stopWord);

    if (index >= 0) {
      this.stopWords.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.stopWords.push(event.option.value);
    this.stopWordInput.nativeElement.value = '';
    this.swCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allStopWordsLang.filter(
      sw => sw.toLowerCase().indexOf(filterValue) === 0
    );
  }

  public onSelectCol($evnt: MatSelectChange): void {
    Object.keys(
      this.indexService.currentIndex.mapping.mappings.data.properties
    ).forEach(key => this.setSearchable(false, key));
    $evnt.value.forEach(element => this.setSearchable(true, element));
    this.searchAbleAttr = $evnt.value;
  }

  public onSelectionRnk($evnt: MatSelectChange): void {
    this.rankings = $evnt.value;
  }

  public resetCriteria(): void {
    this.rankingCriterias = CRITERIA;
  }

  public removeRank(criteria: string): void {
    this.rankingCriterias = this.rankingCriterias.filter(
      data => data !== criteria
    );
  }

  public save(): void {
    this.indexService.currentIndex.setting.settings.index.analysis.filter.my_stop = {
      type: 'standard',
      stopwords: this.stopWords.length === 0 ? '_none_' : this.stopWords
    };
    this.saveTypoValues();

    console.log(this.indexService.currentIndex);
  }
  private buildForm(): void {
    this.form = new FormGroup({
      searchableColumns: new FormControl(undefined),
      rankColumns: new FormControl(undefined),
      pre_length: new FormControl('0'),
      max_expansions: new FormControl('0'),
      trans_pos: new FormControl(false),
      fuzziness: new FormControl('AUTO'),
      ignore_plurals: new FormControl(undefined),
      typoTolerance: new FormControl(false, Validators.required),
      remove_stopwords: new FormControl(undefined, Validators.required),
      disableTypo: new FormControl(undefined),
      optionalWords: new FormControl(undefined, Validators.required)
    });
  }

  setSearchable(flag: boolean, field: string): void {
    this.indexService.currentIndex.mapping.mappings.data.properties[
      field
    ].enabled = flag;
  }
  onChange(event: any): void {
    this.isRemoveStopwords = event.checked;
  }

  isTypo(event: any): void {
    if (event.value) {
      this.typoEnable();
    } else {
      this.typoDisable();
    }
  }

  typoEnable(): void {
    this.form.get('fuzziness').enable();
    this.form.get('pre_length').enable();
    this.form.get('max_expansions').enable();
    this.form.get('trans_pos').enable();
  }

  typoDisable(): void {
    this.form.get('fuzziness').disable();
    this.form.get('pre_length').disable();
    this.form.get('max_expansions').disable();
    this.form.get('trans_pos').disable();
  }

  saveTypoValues(): void {
    this.indexService.currentIndex.queryMeta = {
      query: {
        multi_match: {
          fields: this.indexService.currentDBMeta.columns,
          query: '',
          fuzziness: this.form.get('fuzziness').value,
          prefix_length: this.form.get('pre_length').value,
          max_expansions: this.form.get('max_expansions').value,
          transpositions: this.form.get('trans_pos').value
        }
      }
    };
  }
}
