import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/user.model';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { UtilService } from 'src/app/services/util/util.service';
import { UpdatePasswordComponent } from './update-password/update-password.component';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})

export class ViewProfileComponent implements OnInit {

  public user: User;
  public errorMsg = undefined;
  form: FormGroup;

    // tslint:disable-next-line:no-inferrable-types
    readonly: boolean = false;

    // tslint:disable-next-line:no-inferrable-types
    disabled: boolean = true;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private cookieService: CookieService,
    private utilService: UtilService
  ) {
    if (this.cookieService.check('current_user')) {
      this.user = JSON.parse(cookieService.get('current_user'));
    }
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      user_id: [this.user.user_id, Validators.required],
      username: [this.user.username, [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      email: [this.user.email, Validators.required],
      company: [this.user.company, Validators.required],
      designation: [this.user.designation, Validators.required],
      phone: [this.user.phone, Validators.required],
      address: [this.user.address, Validators.required]
    });


  }

  public onSubmit(): void {
        if (!this.errorMsg && !this.form.invalid) {
          this.form.value.is_enable = this.user.is_enable;
          this.form.value.purpose_of_usage = this.user.purpose_of_usage;
          this.disabled = !this.disabled;
          this.userService.updateUserSimple(this.form.value).subscribe(
            response => {
              this.cookieService.set('current_user', JSON.stringify(this.form.value));
          },
          () => {
            // this.blockButton = false;
          }
        );
      } else {
        this.markFields();
      }


  }

  updatePassword() {
    const config = {
      width: '500px',
      disableClose: false,
      data: {
        component: UpdatePasswordComponent
      }
    };

    this.utilService.createDialog('CUSTOM', config);

  }

  update(el: Element, value: string, control: string) {
    const copy = this.user;
    this.form.controls[control].setValue(value);
  }

  public markFields(): void {
    Object.keys(this.form.controls).forEach(field => {
      const control = this.form.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }
  public onReset(): void {
    // this.form.disable();
     this.form.reset();
   }

}
