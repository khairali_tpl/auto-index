export class Role {
  role_id: number;
  role_name: String;
  role_description: String;
  is_enable: Boolean;
  time_stamp: String;
}
