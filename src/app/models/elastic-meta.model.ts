import { DbInfo } from './db-info.model';
import { User } from './user.model';

export interface ElasticMeta {
  indexName: string;
  host: string;
  port: string;
  username: string;
  password: string;
  clusterName: string;
  indexMeta: string;
  setting: any;
  queryMeta: any;
  mapping: any;
  shards: string;
  replicas: string;
  databaseInfo: DbInfo;
  databaseMeta: DbInfo;
  documentSize: number;
  user: User;
  lastUpdateOn: any;
}
